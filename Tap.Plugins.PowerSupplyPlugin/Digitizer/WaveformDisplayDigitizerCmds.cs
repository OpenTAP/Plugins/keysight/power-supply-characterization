﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.Digitizer
{
    public class WaveformDisplayDigitizerCmds
    {
        public IWaveformDisplayDigitizerCmds channelCmds;

        public WaveformDisplayDigitizerCmds(IWaveformDisplayDigitizerCmds _channelCmds)
        {
            channelCmds = _channelCmds;
        }

        public void ZoomToFullWaveform()
        {
            double ppValue;
            double maxValue;

            ppValue = channelCmds.GetPeakToPeak();
            maxValue = channelCmds.GetMaxVal();

            if (ppValue < 1E36)
            {
                channelCmds.SetVerticalScale(ppValue / 6);
                channelCmds.SetVerticalOffset(maxValue - 0.5 * ppValue);
            }
            else
            {

            }

        }

        public void SetWaveformToFullScreen(double minValue, double maxValue)
        {
            channelCmds.SetVerticalScale((maxValue - minValue) / 6);
            channelCmds.SetVerticalOffset((maxValue-minValue)*0.5+minValue);
        }


        //Methods which combine several SCPIs
        /// <summary>
        /// Scales to the noise and measures its peak to peak level
        /// </summary>
        /// <returns>
        /// Peak to peak level of the voltage
        /// </returns>
        public double GetNoise()
        {
            double ptp = 0;     //Stands for peak to peak
            double scaling = 2.56;
            double minscale = 0.0099;     //Assume a minimum scaling of 0.01 Volts

            while (ptp < 3.5 * scaling && scaling >= minscale)           //While the peak to peak value is smaller than 4 div
            {
                channelCmds.SetVerticalScale(scaling);
                channelCmds.MeasurePeakToPeak(CustomSettings.MeasureSourceSelection.VOLTage);
                ptp = channelCmds.GetPeakToPeak();

                scaling = scaling / 2;
            }

            return ptp;
        }
    }
}