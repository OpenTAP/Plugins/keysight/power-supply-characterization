﻿using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Digitizer;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentChannels
{
    public interface IWaveformDisplayDigitizerCmds : IMultimeterDigitizerCmds
    {
        WaveformDisplayDigitizerCmds WaveformCmds { get; set; }

        void Autoscale();

        //Channel Subsystem

        /// <summary>
        /// Displays or hides the selected channel.
        /// Uses the "CHANnel-n-:CURRent:DISPlay -state-" command.
        /// </summary>
        /// <param name="type">
        /// Unit which is going to be hidden or displayd.
        /// </param>
        /// <param name="state">
        /// New state of the Unit to be displayed (true -> on, false -> off).
        /// </param>
        void SetInput(bool state);

        /// <summary>
        /// Specifies the channel's coupling.
        /// </summary>
        /// <param name="type">
        /// Coupling type to set (AC or DC)
        /// </param>
        void SetCoupling(CustomSettings.CouplingSelection type);

        /// <summary>
        /// Sets the vertival scaling
        /// </summary>
        /// <param name="scale">
        /// Scaling in Volts/div.
        /// </param>
        void SetVerticalScale(double scale);

        /// <summary>
        /// Gets the actual scaling
        /// </summary>
        /// <returns>
        /// The actual scaling in Volts/div.
        /// </returns>
        double GetVerticalScale();

        /// <summary>
        /// Sets the value that is represented at center screen for the specified channel.
        /// </summary>
        /// <param name="offset">
        /// Offset in Volts.
        /// </param>
        void SetVerticalOffset(double offset);

        void SetTriggerSource();

        void SetTriggerLevel(double level);

        void ViewSource();

        void SetAntiAliasing(bool state);

        //Measure subsystem
        /// <summary>
        /// Returns the waveform's average (arithmetic mean) value.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        double GetAverage();

        /// <summary>
        /// Get the maximum value on the screen.
        /// </summary>
        /// <param name="type">
        /// Set Unit from which the maximum value needs to be determined.
        /// </param>
        /// <returns>
        /// The Maximum measured value.
        /// </returns>
        double GetMaxVal();

        /// <summary>
        /// Measures the Falltime of the displayed Signal and shows it on the screen.
        /// </summary>
        /// <param name="type">
        /// Measurable unit which Falltime is going to be determined.
        /// </param>
        void MeasureFalltime();

        /// <summary>
        /// Measures the waveform's peak-to-peak value.
        /// </summary>
        /// <param name="type"></param>
        void MeasurePeakToPeak(CustomSettings.MeasureSourceSelection type);

        /// <summary>
        /// Sets the measurement thresholds in the order upper, middle, lower.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="upper"></param>
        /// <param name="middle"></param>
        /// <param name="lower"></param>
        void SetThresholds(double upper, double middle, double lower);

        /// <summary>
        /// Sets the default sources for measurements.
        /// </summary>
        /// <param name="type"></param>
        void SetMeasurementSource(CustomSettings.MeasureSourceSelection type);

        /// <summary>
        /// Returns the time in seconds from the upper threshold to the lower threshold.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        double GetFalltime();

        /// <summary>
        /// Returns the waveform's peak-to-peak value.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        double GetPeakToPeak();

        /// <summary>
        /// Measures the delay time in seconds between source1 and source2 edges.
        /// </summary>
        /// <param name="secondPA"></param>
        /// <param name="source1"></param>
        /// <param name="source2"></param>
        void SetDelaytime(CustomSettings.MeasureSourceSelection source1, CustomSettings.MeasureSourceSelection source2, CustomSettings.ScopeEdgeSelectMode edgeSelect);

        /// <summary>
        /// Measures the time in seconds from the upper threshold to the lower threshold.
        /// </summary>
        /// <param name="type"></param>
        void SetFalltime();

        /// <summary>
        /// Measures the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        /// <param name="type"></param>
        void SetRisetime();

        /// <summary>
        /// Returns the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        double GetRisetime();

        double PerformStandardDeviationMeaurement();

        //Get a Waveform
        waveform GetWaveformData(int points);

        void RangeToFullWaveform();

        double PerformHarmonicsAnalysis();
    }
}
