﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.Digitizer
{
    public abstract class DigitizerCmds
    {
        public ChanCmds channelCmds;

        public DigitizerCmds(ChanCmds _channelCmds)
        {
            this.channelCmds = _channelCmds;
        }
    }
}
