﻿using Agilent.CommandExpert.ScpiNet.AgInfiniiVision3000X_7_11;
// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Digitizer;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    /// <summary>
    /// Represents an (Keysight) oscilloscope
    /// This can be a benchtop Oscilloscope or a network/USB oscilloscope
    /// </summary>
    public class ScopeDigitizerCmds: DigitizerCmds, IWaveformDisplayDigitizerCmds
    {
        //Defines the type of the Oscilloscope channel.
        //For example it can be an analog channel, a digital channel, or or a virtual channel (Math function)
        CustomSettings.ScopeInputSelection channelType;
        new public ScopeChanCmds channelCmds { get; set; }

        public WaveformDisplayDigitizerCmds WaveformCmds { get; set; }

        //Constructor
        public ScopeDigitizerCmds(ScopeChanCmds _channelCmds) : base(_channelCmds)
        {
            channelCmds = _channelCmds;

            WaveformCmds = new WaveformDisplayDigitizerCmds(this);
        }

        //Root Subsystem
        public void Autoscale()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.AUToscale.Command("CHANnel" + this.channelCmds.channel);
        }

        /// <summary>
        /// Gets the channelType property of the InstrumentChannel object.
        /// </summary>
        /// <returns>
        /// The channelType property of the InstrumentChannel object.
        /// Identifies wheter the channel is a analog channel, a digital channel or a Math function (virtual channel) or something else
        /// </returns>
        public CustomSettings.ScopeInputSelection GetChannelType()
        {
            return this.channelType;
        }

        /// <summary>
        /// Sets the channelType property of the InstrumentChannel object.
        /// </summary>
        /// <param name="channelType">
        /// The channelType property of the InstrumentChannel object.
        /// Identifies wheter the channel is a analog channel, a digital channel or a Math function (virtual channel) or something else
        /// </param>
        public void SetChannelType(CustomSettings.ScopeInputSelection channelType)
        {
            this.channelType = channelType;
        }

        //Methods which use SCPIs to send commands and queries to instruments.
        //Root Commands

        /// <summary>
        /// Performs the Autoscale Command
        /// </summary>
        /// <param name="showOption">
        /// When ALL is selected, all channels that meet the requirements of :AUToscale will be displayed.
        /// When DISPlayed is selected, only the channels that are turned on are autoscaled.
        /// </param>
        public void Autoscale(CustomSettings.ShowOption showOption)
        {
            
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.AUToscale.CHANnels.Command(showOption.ToString("g"));
        }

        /// <summary>
        /// Enables the input and shows a specified source.
        /// </summary>
        public void ViewSource()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.VIEW.Command(channelType.ToString("g") + this.channelCmds.channel.ToString());

            TestPlan.Sleep(1000);       //Wait a second because the command will not return anything
        }

        /// <summary>
        /// Stops display of the specified wafeform
        /// </summary>
        public void BlankSource()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.BLANk.Command(channelType.ToString("g") + this.channelCmds.channel.ToString());
            
            TestPlan.Sleep(1000);       //Wait a second because the command will not return anything
        }

        //Channel Subsystem

        /// <summary>
        /// Displays or hides the selected channel.
        /// Uses the "CHANnel-n-:CURRent:DISPlay -state-" command.
        /// </summary>
        /// <param name="type">
        /// Unit which is going to be hidden or displayd.
        /// </param>
        /// <param name="state">
        /// New state of the Unit to be displayed (true -> on, false -> off).
        /// </param>
        public void SetInput(bool state)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.DISPlay.Command(this.channelCmds.channel, state);
        }

        /// <summary>
        /// Specifies the channel's coupling.
        /// </summary>
        /// <param name="type">
        /// Coupling type to set (AC or DC)
        /// </param>
        public void SetCoupling(CustomSettings.CouplingSelection type)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.COUPling.Command(this.channelCmds.channel, type.ToString("g"));
        }

        public enum Unit { VOLT, AMPere};
        public Unit GetUnit()
        {
            Unit unitEnum = Unit.VOLT;
            string unitStr;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.UNITs.Query(this.channelCmds.channel, out unitStr);

            if(unitStr=="VOLT")
            {
                unitEnum = Unit.VOLT;
            }
            else if(unitStr == "AMP")
            {
                unitEnum = Unit.AMPere;
            }

            return unitEnum;
        }

        /// <summary>
        /// Sets the vertival scaling
        /// </summary>
        /// <param name="scale">
        /// Scaling in Volts/div.
        /// </param>
        public void SetVerticalScale(double scale)
        {
            if (channelCmds.InstrumentCommands.device.GetType() == typeof(AgInfiniiVision3000X))
            {
                double minScale = 0;
                double maxScale = 0;
                double attenuation;
                
                ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.PROBe.Query(this.channelCmds.channel, out attenuation);

                if (this.GetImpedance() == InputImpedance.ONEMeg)
                {
                    if (this.GetUnit() == Unit.VOLT)
                    {
                        minScale = 0.001 * Math.Round(attenuation);
                        maxScale = 5 * Math.Round(attenuation);
                    }
                    if (this.GetUnit() == Unit.AMPere)
                    {
                        minScale = 0.025 / Math.Round(attenuation);
                        maxScale = 6.25 / Math.Round(attenuation);
                    }
                }
                else
                {
                    if (this.GetUnit() == Unit.VOLT)
                    {
                        minScale = 0.001 * Math.Round(attenuation);
                        maxScale = 1 * Math.Round(attenuation);
                    }
                    if (this.GetUnit() == Unit.AMPere)
                    {
                        throw new NotSupportedException();
                    }
                }

                if (scale < minScale)
                {
                    ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.SCALe.Command(this.channelCmds.channel, minScale);
                }
                else if(scale > maxScale)
                {
                    ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.SCALe.Command(this.channelCmds.channel, maxScale);
                }
                else
                {
                    ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.SCALe.Command(this.channelCmds.channel, scale);
                }
            }
            else
            {
                throw new Exception();
            }
            TestPlan.Sleep(1000);
        }


        public enum InputImpedance { ONEMeg, FIFTy}
        public InputImpedance GetImpedance()
        {
            InputImpedance returnEnum;
            string impedanceValue = "";

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.IMPedance.Query(this.channelCmds.channel, out impedanceValue);

            if(impedanceValue == "ONEM")
            {
                returnEnum = InputImpedance.ONEMeg;
            }
            else if(impedanceValue == "FIFT")
            {
                returnEnum = InputImpedance.FIFTy;
            }
            else
            {
                throw new Exception();
            }

            return returnEnum;
        }


        /// <summary>
        /// Gets the actual scaling
        /// </summary>
        /// <returns>
        /// The actual scaling in Volts/div.
        /// </returns>
        public double GetVerticalScale()
        {
            double returnvalue;
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.SCALe.Query(this.channelCmds.channel, out returnvalue);
            return returnvalue;
        }

        /// <summary>
        /// Sets the value that is represented at center screen for the specified channel.
        /// </summary>
        /// <param name="offset">
        /// Offset in Volts.
        /// </param>
        public void SetVerticalOffset(double offset)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.OFFSet.Command(this.channelCmds.channel, offset);
        }

        /// <summary>
        /// Sets the Hardwarerange of the Instrument.
        /// </summary>
        /// <param name="range">
        /// Maximum measurable value to set in Volts.
        /// </param>
        public void SetDisplayRange(double range)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.CHANnel.RANGe.Command(this.channelCmds.channel, range);
        }

        /// <summary>
        /// Enables or disables antialiasing.
        /// </summary>
        /// <param name="state">
        /// true to enable antialiasing
        /// false to disable antialiasing
        /// </param>
        public void SetAntiAliasing(bool state)
        {
            if (state == true)
            {
                ((AgInfiniiVision3000X)this.channelCmds.InstrumentCommands.device).SCPI.ACQuire.DAALias.Command("AUTO");
            }
            else
            {
                ((AgInfiniiVision3000X)this.channelCmds.InstrumentCommands.device).SCPI.ACQuire.DAALias.Command("DISable");
            }
        }

        //Trigger Subsystem

        /// <summary>
        /// Selects the InstrumentChannel to trigger the Power Analyzer.
        /// </summary>
        public void SetTriggerSource()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.TRIGger.EDGE.SOURce.Command("CHANnel" + this.channelCmds.channel);
        }

        /// <summary>
        /// Sets the voltage, current or powerlevel which invokes the trigger
        /// </summary>
        /// <param name="level">
        /// Level to be set in Volts.
        /// </param>
        public void SetTriggerLevel(double level)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.TRIGger.EDGE.LEVel.Command(level, "CHANnel" + this.channelCmds.channel.ToString());
        }

        //Measure subsystem

        /// <summary>
        /// Returns the waveform's average (arithmetic mean) value.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetAverage()
        {
            double value;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VAVerage.Query(out value);

            return value;
        }

        /// <summary>
        /// Get the maximum value on the screen.
        /// </summary>
        /// <param name="type">
        /// Unity of the signal which is measured (Current, Voltage, Power)
        /// Does not apply for Oscilloscopes and can be left free.
        /// </param>
        /// <returns>
        /// The Maximum measured value.
        /// </returns>
        public double GetMaxVal()
        {
            double returnvalue;

            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VMAX.Command(channelType.ToString("g") + this.channelCmds.channel);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VMAX.Query(out returnvalue);

            return returnvalue;
        }

        /// <summary>
        /// Measures the Falltime of the displayed Signal and shows it on the screen.
        /// </summary>
        public void MeasureFalltime()
        {
            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2 || channelType == CustomSettings.ScopeInputSelection.FFT)
            {
                throw new Exception();              //Chosen option is not available
            }

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.FALLtime.Command(channelType.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Measures the waveform's peak-to-peak value.
        /// </summary>
        public void MeasurePeakToPeak(CustomSettings.MeasureSourceSelection type = CustomSettings.MeasureSourceSelection.VOLTage)
        {
            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VPP.Command(channelType.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Sets the measurement thresholds in the order upper, middle, lower.
        /// </summary>
        /// <param name="upper"></param>
        /// <param name="middle"></param>
        /// <param name="lower"></param>
        public void SetThresholds(double upper, double middle, double lower)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.DEFine.CommandThresholds("THResholds", "PERCent", upper.ToString(), middle, lower);
        }

        /// <summary>
        /// Sets the default sources for measurements.
        /// </summary>
        public void SetMeasurementSource(CustomSettings.MeasureSourceSelection type = CustomSettings.MeasureSourceSelection.VOLTage)          //External is not implemented yet
        {
            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.SOURce.Command(channelType.ToString("g"), this.channelCmds.channel);
        }

        /// <summary>
        /// Returns the time in seconds between the upper threshold to the lower threshold.
        /// </summary>
        /// <returns>
        /// The falltime in seconds.
        /// </returns>
        public double GetFalltime()
        {
            double returnvalue;

            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2 || channelType == CustomSettings.ScopeInputSelection.FFT)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.FALLtime.Query(out returnvalue);

            return returnvalue;
        }

        /// <summary>
        /// Returns the waveform's peak-to-peak value.
        /// </summary>
        /// <returns>
        /// The peak-to-peak value of the waveform.
        /// </returns>
        public double GetPeakToPeak()
        {
            double returnvalue;

            if (channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VPP.Command("CHANnel" + this.channelCmds.channel);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VPP.Query(out returnvalue);

            return returnvalue;
        }

        /// <summary>
        /// Measures the delay time in seconds between source1 and source2 edges.
        /// </summary>
        /// <param name="secondPA"></param>
        /// <param name="source1"></param>
        /// <param name="source2"></param>
        public void SetDelaytime(CustomSettings.MeasureSourceSelection source1, CustomSettings.MeasureSourceSelection source2, CustomSettings.ScopeEdgeSelectMode edgeSelect)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.DELay.Command(edgeSelect.ToString("g"), source1.ToString("g"), source2.ToString("g"));
        }
        
        /// <summary>
        /// Measures the time in seconds from the upper threshold to the lower threshold.
        /// </summary>
        public void SetFalltime()
        {
            if (channelType == CustomSettings.ScopeInputSelection.FFT || channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.FALLtime.Command(channelType.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Measures the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        public void SetRisetime()
        {
            if (channelType == CustomSettings.ScopeInputSelection.FFT || channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.RISetime.Command(channelType.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Returns the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        /// <returns></returns>
        public double GetRisetime()
        {
            double returnvalue;

            if (channelType == CustomSettings.ScopeInputSelection.FFT || channelType == CustomSettings.ScopeInputSelection.SBUS1 || channelType == CustomSettings.ScopeInputSelection.SBUS2)
            {
                throw new Exception();              //Chosen option is not available
            }
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.RISetime.Query(out returnvalue);

            return returnvalue;
        }

        public double GetRmsValue()
        {
            double returnvalue;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.VRMS.Query("DISPlay", "DC", "CHANnel" + this.channelCmds.channel, out returnvalue);

            return returnvalue;
        }

        public double GetRippleAnalysis()
        {
            double returnValue = 0;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.RIPPle.Query(out returnValue);

            return returnValue;
        }

        public double GetStandardDeviation()
        {
            double returnValue = 0;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MEASure.SDEViation.Query(out returnValue);

            return returnValue;
        }

        //Power Subsystem
        public void ApplyRippleAnalysis()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.RIPPle.APPLy.Command();
        }
        public double PerformStandardDeviationMeaurement()
        {
            this.SetPowerApplicationVoltage();
            this.PowerSignalsAutoSetup(ScopeDigitizerCmds.specialValues.RIPPle);
            this.ApplyRippleAnalysis();
            return this.GetStandardDeviation();
        }

        public enum specialValues { HARMonics, DESKew, EFFiciency, RIPPle, MODulation, QUALity, SLEW, RDSVce, SWITch }
        public void PowerSignalsAutoSetup(specialValues spec)
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.SIGNals.AUTosetup.Command(spec.ToString("g"));
        }

        //Marker subsystem
        /// <summary>
        /// If MARKer:MODE is not presently set to WAVEform, this command sets MARKer:MODE to MANual and sets the cursor position to the specified value.
        /// </summary>
        /// <returns>
        /// The value of the cursor.
        /// </returns>
        public double GetMarkerX1()
        {
            double position;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MARKer.X1.DISPlay.Command(true);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MARKer.X1Position.Query(out position);

            return position;
        }

        /// <summary>
        /// If MARKer:MODE is not presently set to WAVEform, this command sets MARKer:MODE to MANual and sets the cursor position to the specified value.
        /// </summary>
        /// <returns>
        /// The value of the cursor.
        /// </returns>
        public double GetMarkerX2()
        {
            double position;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MARKer.X2.DISPlay.Command(true);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.MARKer.X2Position.Query(out position);

            return position;
        }

        public void SetPowerApplicationVoltage()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.SIGNals.SOURce.VOLTage.Command(1, "CHANnel" + this.channelCmds.channel);
        }

        public void SetPowerApplicationCurrent()
        {
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.SIGNals.SOURce.CURRent.Command(1, "CHANnel" + this.channelCmds.channel);
        }

        public double PerformHarmonicsAnalysis()
        {
            double THD;

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.ENABle.Command("ON");

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.HARMonics.LINE.Command("AUTO");

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.HARMonics.APPLy.Command();

            TestPlan.Sleep(2000);

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.POWer.HARMonics.THD.Query(out THD);

            return THD;
        }


        //Get a Waveform
        public waveform GetWaveformData(int _points)
        {
            int points;
            waveform wave = new waveform();

            int maxPoints = this.channelCmds.InstrumentCommands.GetAcquiredPoints();
            
            if(_points > maxPoints)
            {
                points = maxPoints;
            }
            else
            {
                points = _points;
            }

            double screenlength = this.channelCmds.InstrumentCommands.GetTimebaseScale() * 10;

            double[] returnarray = new double[points];
            List<double> returnList = new List<double> { };

            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.WAVeform.SOURce.Command("CHANnel" + this.channelCmds.channel);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.WAVeform.POINts.Command(points);
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.WAVeform.FORMat.Command("ASCii");
            ((AgInfiniiVision3000X)channelCmds.InstrumentCommands.device).SCPI.WAVeform.DATA.QueryAsciiReal(out returnarray);

            wave.triggerTime = this.channelCmds.InstrumentCommands.GetTimebasePosition();

            wave.yAxisValues = returnarray.ToList<double>();
            points = wave.yAxisValues.Count();

            double timeBetweenPoints = screenlength / points;
            for (int i = 0; i < points; i++)
            {
                wave.timeAxisValues.Add(((double)i) * timeBetweenPoints);
            }

            return wave;
        }

        public void RangeToFullWaveform()
        {
            this.WaveformCmds.ZoomToFullWaveform();     //Is for Scopes the same as the Waveform.ZoomToFullWaveform command
        }
    }
}