﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.Digitizer
{
    public class PAPOWerDigitizerCmds : PADigitizerCmds, IWaveformDisplayDigitizerCmds, IACPowerDigitizer, IDCPowerDigitizer
    {
        public PAPOWerDigitizerCmds(PAChanCmds _channelCmds) : base(_channelCmds, CustomSettings.InputSelection.POWer)
        {

        }

        public double GetAvgdPower()
        {
            return this.GetRmsValue();
        }

        public void StartPowerQualityMeasurement()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("ANALyze:QUALity" + this.channelCmds.channel + ":MEASure");
        }

        public double GetAparentACDCPower()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("ANALyze:QUALity"+ this.channelCmds.channel +":POWer:APPArent:AC:RMS:DC?");

            return double.Parse(str);
        }

        public double GetRealACDCPower()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("ANALyze:QUALity"+ this.channelCmds.channel +":POWer:REAL:AC:RMS:DC?");

            return double.Parse(str);
        }

        public double GetRealACPower()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("ANALyze:QUALity" + this.channelCmds.channel + ":POWer:REAL:AC:RMS?");

            return double.Parse(str);
        }

        public double GetRealDCPower()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("ANALyze:QUALity" + this.channelCmds.channel + ":POWer:REAL:DC?");

            return double.Parse(str);
        }
    }
}
