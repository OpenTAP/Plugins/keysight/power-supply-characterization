﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Digitizer;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public class PADigitizerCmds : DigitizerCmds , IWaveformDisplayDigitizerCmds
    {
        public enum CurrentInputType { A2, A50 }
        public CurrentInputType CurrentInput { get; set; }

        CustomSettings.InputSelection inputType;

        new public PAChanCmds channelCmds { get; set; }

        public WaveformDisplayDigitizerCmds WaveformCmds { get; set; }
        
        //Constructor
        public PADigitizerCmds(PAChanCmds _channelCmds,CustomSettings.InputSelection input):base(_channelCmds)
        {
            this.channelCmds = _channelCmds;
            this.inputType = input;

            WaveformCmds = new WaveformDisplayDigitizerCmds(this);
        }


        //Methods which use SCPIs to send commands and queries to instruments.
        //Root Commands

        /// <summary>
        /// Performs the Autoscale Command
        /// </summary>
        /// <param name="type">
        /// Determines the unit which needs to be displayed properly.
        /// </param>
        public void Autoscale()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("AUToscale " + inputType.ToString("g") + channelCmds.channel.ToString());
        }

        /// <summary>
        /// Enables the input and shows a specified source.
        /// </summary>
        /// <param name="unit">
        /// Specifies the unit which is going to bedisplayed.
        /// </param>
        public void ViewSource()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("VIEW " + inputType.ToString("g") + this.channelCmds.channel );
            TestPlan.Sleep(1000);       //Wait a second because the command will not return anything
        }

        /// <summary>
        /// Stops display of the specified wafeform
        /// </summary>
        public void BlankSource()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("BLANk "+ this.channelCmds.channel.ToString());
            TestPlan.Sleep(1000);       //Wait a second because the command will not return anything
        }

        /// <summary>
        /// Sets the Instrumment into single trigger mode.
        /// </summary>
        public void SetSingle()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand(":SINGle");
            TestPlan.Sleep(2000);
        }


        //Channel Subsystem

        /// <summary>
        /// Displays or hides the selected channel.
        /// Uses the "CHANnel-n-:CURRent:DISPlay -state-" command.
        /// </summary>
        /// <param name="type">
        /// Unit which is going to be hidden or displayd.
        /// </param>
        /// <param name="state">
        /// New state of the Unit to be displayed.
        /// </param>
        public void SetInput( bool state)
        {
            string strstate;

            if (state == true)
            {
                strstate = "ON";
            }
            else
            {
                strstate = "OFF";
            }

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel.ToString() + ":" + inputType.ToString("g") + ":DISPlay " + strstate);
        }

        /// <summary>
        /// Specifies the channel's coupling.
        /// </summary>
        /// <param name="type">
        /// Coupling type to set (AC or DC)
        /// </param>
        public void SetCoupling(CustomSettings.CouplingSelection type)
        {
            channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":COUPling " + type.ToString("g"));
        }

        /// <summary>
        /// Enables or disables antialiasing.
        /// </summary>
        /// <param name="state">
        /// true to enable antialiasing
        /// false to disable antialiasing
        /// </param>
        public void SetAntiAliasing(bool state)
        {
            string strstate;

            if (state == true)
            {
                strstate = "AAON";
            }
            else
            {
                strstate = "AAOFf";
            }

            channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":ANTialias " + strstate);
        }

        /// <summary>
        /// Enables or disables the Autorange functionality
        /// </summary>
        /// <param name="state">
        /// New state of the Autorange functionality (true: on, false: off)
        /// </param>
        public void SetAutorange(bool state)
        {
            string strstate;

            if (state)
            {
                strstate = "ON";
            }
            else
            {
                strstate = "OFF";
            }

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":AUTorange " + strstate);
        }

        /// <summary>
        /// Sets the vertival scaling
        /// </summary>
        /// <param name="unity">
        /// Unitiy which needs to be scaled
        /// </param>
        /// <param name="scale">
        /// Scaling in Volts/div, Amperes/div, or Watts/div.
        /// </param>
        public void SetVerticalScale(double scale)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":" + inputType.ToString("g") + ":SCALe " + scale.ToString());
            TestPlan.Sleep(1000);
        }

        /// <summary>
        /// Gets the actual scaling
        /// </summary>
        /// <param name="unity">
        /// Scaling in Volts, Amperes or Watts per div
        /// </param>
        /// <returns></returns>
        public double GetVerticalScale()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("CHANnel" + this.channelCmds.channel + ":" + inputType.ToString("g") + ":SCALe?");

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// Sets the value that is represented at center screen for the specified channel.
        /// </summary>
        /// <param name="unit">
        /// Unit of the trace to set the offset for.
        /// </param>
        /// <param name="offset">
        /// Offset in Volts, Amperes or Watts.
        /// </param>
        public void SetVerticalOffset(double offset)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":" + inputType.ToString("g") + ":OFFSet " + offset.ToString());
        }

        /// <summary>
        /// Sets the Hardwarerange of the Instrument.
        /// </summary>
        /// <param name="type">
        /// Measurable unit which is going to be limited.
        /// </param>
        /// <param name="range">
        /// Maximum measurable value to set in Volts, Amperes, or Watts.
        /// </param>
        public void SetHWRange(double range)
        {
            string strrange = range.ToString();

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":" + inputType.ToString("g") + ":RANGe:HW " + strrange);
        }

        /// <summary>
        /// Sets the HW range (in Volts or Amperes) for measurements.
        /// </summary>
        /// <param name="type">
        /// Unit to set the range for (Ampere or Volt)
        /// </param>
        /// <param name="range">
        /// Range to set
        /// </param>
        public void SetDisplayRange(double range, CustomSettings.VoltCurrSelection type)
        {
            string strrange = range.ToString();

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":" + type.ToString("g") + ":RANGe:DISPlay " + strrange);
        }

        /// <summary>
        /// Specifies which current input to use.
        /// </summary>
        public void SetCurrentInput()
        {
            if(this.CurrentInput == CurrentInputType.A2)
            {
                this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":CURRent:INPUt:SELEct A2");
            }
            else if(this.CurrentInput == CurrentInputType.A50)
            {
                this.channelCmds.InstrumentCommands.instrument.ScpiCommand("CHANnel" + this.channelCmds.channel + ":CURRent:INPUt:SELEct A50");
            }
        }

        //Trigger Subsystem

        /// <summary>
        /// Selects the InstrumentChannel to trigger the Power Analyzer.
        /// </summary>
        /// <param name="type">
        /// Measurable unit which shall be used to trigger the Power Analyzer.
        /// </param>
        public void SetTriggerSource()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand(":TRIGger:SOURce " + inputType.ToString("g") + this.channelCmds.channel);
        }
        /// <summary>
        /// Sets the voltage, current or powerlevel which invokes the trigger
        /// </summary>
        /// <param name="level">
        /// Level to be set in Volts, Ameres or Watts.
        /// </param>
        public void SetTriggerLevel(double level)
        {
            string strlevel = level.ToString();

            this.SetTriggerSource();
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("TRIGger:LEVel " + strlevel);
        }

        //Measure subsystem
        /// <summary>
        /// Returns the waveform's average (arithmetic mean) value.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetAverage()
        {
            string str;

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:AVERage DISPlay, " + inputType.ToString("g")+ this.channelCmds.channel);
            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:AVERage? DISPlay, "+ inputType.ToString("g")+ this.channelCmds.channel );

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// Get the maximum value on the screen.
        /// </summary>
        /// <param name="type">
        /// Set Unit from which the maximum value needs to be determined.
        /// </param>
        /// <returns>
        /// The Maximum measured value.
        /// </returns>
        public double GetMaxVal()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:MAX? " + inputType.ToString("g") + this.channelCmds.channel);

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// Measures the Falltime of the displayed Signal and shows it on the screen.
        /// </summary>
        /// <param name="type">
        /// Measurable unit which Falltime is going to be determined.
        /// </param>
        public void MeasureFalltime()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:FALLtime " + inputType.ToString("g"));
        }

        /// <summary>
        /// Measures the waveform's peak-to-peak value.
        /// </summary>
        /// <param name="type"></param>
        public void MeasurePeakToPeak(CustomSettings.MeasureSourceSelection type)
        {
            channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:PP " + type.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Sets the measurement thresholds in the order upper, middle, lower.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="upper"></param>
        /// <param name="middle"></param>
        /// <param name="lower"></param>
        public void SetThresholds(double upper, double middle, double lower)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:DEFine THResholds, PERCent, " + upper.ToString() + ", " + middle.ToString() + ", " + lower.ToString());
            this.channelCmds.InstrumentCommands.instrument.WaitForOperationComplete(5000);
        }

        /// <summary>
        /// Sets the default sources for measurements.
        /// </summary>
        /// <param name="type"></param>
        public void SetMeasurementSource(CustomSettings.MeasureSourceSelection type)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:SOURce " + type.ToString("g") + this.channelCmds.channel);
        }

        /// <summary>
        /// Returns the time in seconds from the upper threshold to the lower threshold.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetFalltime()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:FALLtime? " + inputType.ToString("g"));

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// Returns the waveform's peak-to-peak value.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetPeakToPeak()
        {
            string str;

            str = channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:PP? " + inputType.ToString("g") + this.channelCmds.channel);

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// Measures the delay time in seconds between source1 and source2 edges.
        /// </summary>
        /// <param name="secondPA"></param>
        /// <param name="source1"></param>
        /// <param name="source2"></param>
        public void SetDelaytime(CustomSettings.MeasureSourceSelection source1, CustomSettings.MeasureSourceSelection source2, CustomSettings.ScopeEdgeSelectMode edgeSelect = CustomSettings.ScopeEdgeSelectMode.AUTO)
        {
            string strsource1 = source1.ToString("g");
            string strsource2 = source2.ToString("g");

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:DELay " + strsource1 + " ," + strsource2);
        }

        /// <summary>
        /// Measures the time in seconds from the upper threshold to the lower threshold.
        /// </summary>
        /// <param name="type"></param>
        public void SetFalltime()
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:FALLtime " + inputType.ToString("g") + channelCmds.channel);
        }

        /// <summary>
        /// Measures the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        /// <param name="type"></param>
        public void SetRisetime()
        {
            //this.instrument.ScpiCommand("MEASure:RISetime " + CustomSettings.GetStrType(type) + this.channel.ToString());
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("MEASure:RISetime " + inputType.ToString("g") + channelCmds.channel);
        }

        /// <summary>
        /// Returns the time in seconds from the lower threshold to the upper threshold.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public double GetRisetime()
        {
            string str;

            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:RISetime? " + inputType.ToString("g"));

            return Convert.ToDouble(str);
        }

        public double GetRmsValue()
        {
            string str;

            this.RangeToFullWaveform();
            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:RMS? DISPlay ,DC ," + inputType.ToString("g") + this.channelCmds.channel );

            return Convert.ToDouble(str);
        }

        public double PerformStandardDeviationMeaurement()
        {
            string str;

            this.RangeToFullWaveform();
            str = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("MEASure:SDEViation? " + inputType.ToString("g") + this.channelCmds.channel);

            return Convert.ToDouble(str);
        }

        //Analyze subsystem
        /// <summary>
        /// Sets the sync source for the CWQ measurement.
        /// </summary>
        /// <param name="type"></param>
        private void SetCwaSyncSource(CustomSettings.InputSourceSelection type)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("ANALyze:SOURce" + this.channelCmds.channel + ":SYNC " + type.ToString("g"));
        }

        /// <summary>
        /// Sets the source for precompliance harmonics
        /// </summary>
        /// <param name="type"></param>
        public void SetHarmonicsSource(CustomSettings.VoltCurrSelection type)
        {
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("ANALyze:HARMonics:PREcomply:SOURce " + type.ToString("g")+ this.channelCmds.channel );
        }

        /// <summary>
        /// Performs a Harmonics Analysis
        /// </summary>
        /// <param name="frequency">
        /// Frequency for which the harmonics analysis needs to be performed.
        /// </param>
        /// <param name="standard">
        /// Standard after which the harmonics analysis needs to be performed.
        /// </param>
        /// <returns>
        /// Result of the harmonics analysis (currently the total harmonic distorsion).
        /// </returns>
        public double PerformHarmonicsAnalysis()
        {
            ((PACmds)(this.channelCmds.InstrumentCommands)).SetAnalyzeMode(true);
            this.SetHarmonicsSource(CustomSettings.VoltCurrSelection.CURRent);
            ((PACmds)(this.channelCmds.InstrumentCommands)).SetHarmonicsNumber(40);
            ((PACmds)(this.channelCmds.InstrumentCommands)).SetHarmonicsDuration(0.2);
            SetCwaSyncSource(CustomSettings.InputSourceSelection.CURRent);
            
            ((PACmds)(this.channelCmds.InstrumentCommands)).MeasHarmonics();

            TestPlan.Sleep(2000);

            return ((PACmds)(this.channelCmds.InstrumentCommands)).GetHarmonicsTHD();

            //May be implemented later
            /*
            GetHarmonicsStatus();
            GetHarmonicsData();
            */
        }


        //Get a Waveform
        public waveform GetWaveformData(int _points)
        {
            int points;
            waveform wave = new waveform();
            int maxPoints = this.channelCmds.InstrumentCommands.GetAcquiredPoints() - 1;            //The firs value is thrown away later.
            if( _points > maxPoints )
            {
                points = maxPoints;
            }
            else
            {
                points = _points;
            }
           
            double screenlength = this.channelCmds.InstrumentCommands.GetTimebaseScale() * 10;

            string fullStr;

            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("WAVeform:SOURce " + inputType.ToString() + this.channelCmds.channel);
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("WAVeform:POINts " + points);
            this.channelCmds.InstrumentCommands.instrument.ScpiCommand("WAVeform:FORMat ASCii");
            fullStr = this.channelCmds.InstrumentCommands.instrument.ScpiQuery("WAVeform:DATA?");

            string strPart = "";
            foreach (char c in fullStr)
            {
                if (c == ',')
                {
                    try
                    {
                        wave.yAxisValues.Add(double.Parse(strPart));
                    }
                    catch
                    {
                        //The returned value starts with #[some number] [first value]
                        //For this reason the first value is invalid
                    }
                    strPart = "";
                }
                else
                {
                    strPart = strPart + c;
                }
            }
            wave.triggerTime = this.channelCmds.InstrumentCommands.GetTimebasePosition();

            points = wave.yAxisValues.Count;
            double timeBetweenPoints = screenlength / points;
            for (int i = 0; i < points; i++)
            {
                wave.timeAxisValues.Add(((double)i) * timeBetweenPoints);
            }

            return wave;
        }

        public void RangeToFullWaveform()
        {
            double ppValue;
            double maxValue;

            ppValue = GetPeakToPeak();
            maxValue = GetMaxVal();

            SetHWRange(maxValue);
            SetVerticalScale(ppValue / 6);
            SetVerticalOffset(maxValue - 0.5 * ppValue);
        }
    }
}