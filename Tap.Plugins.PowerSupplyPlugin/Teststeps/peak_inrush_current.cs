﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Peak Inrush Current", Group: "Switching Mode Power Supplies", Description: "Measures the peak inrush current of a device under test when connected to its supply voltage.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class peak_inrush_current : TestStep
    {
        double maxInrushCurrent;
        
        private waveform inputVoltageWave;
        private waveform outputVoltageWave;
        private waveform inputCurrentWave;
        private waveform outputCurrentWave;

        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;

        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;

        #region Settings
        // ToDo: Add property here for each parameter the end user should be able to change
        #endregion
        public peak_inrush_current()
        {
            
        }

        private double pOut, vIn, iIn;
        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;

            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;


            //Assume the efficiency of the DUT is at 100% and use the calculated input Current to determine the trigger level
            pOut = GetParent<Run_PS_Testplan>().MaximumOutputPower;
            vIn = GetParent<Run_PS_Testplan>().InputVoltage;
            iIn = pOut / vIn;

            base.PrePlanRun();
        }
        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            measure(powerAnalyzer);

            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }

            UpgradeVerdict(Verdict.Pass);
        }

        private void measure(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            #region Set Power Analyzer settings

            if(waveformInstrument.InstrumentCmds.GetType() == typeof(PACmds))
            {

                ((PADigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetCurrentInput();
                ((PACmds)(waveformInstrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM3);
                
                ((PADigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetHWRange(2);
            }

            waveformInstrument.InputCurrentDigitizerCmds.ViewSource();
            waveformInstrument.InputVoltageDigitizerCmds.ViewSource();
            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerSource();
            waveformInstrument.InstrumentCmds.SetSlope(CustomSettings.SlopeSelection.POSitive);
            waveformInstrument.InstrumentCmds.SetTriggerType(CustomSettings.TriggerType.EDGE);
            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerLevel(GetParent<Run_PS_Testplan>().InputVoltage * 0.2);

            waveformInstrument.InstrumentCmds.SetTimebaseScale(0.1);      //Timebase is 100 ms/div
            waveformInstrument.InstrumentCmds.SetTimebasePosition(0.4);

            #endregion

            ////////////Run the test with low accuracy/////////////////////////////////////////////////////////////////////////
            //Appy electronic load for predefined input current
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);     //Needs to be set for AC Power Analyzers
            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            //Set the Current Limit for the power supply
            triggerAtPowerOn(waveformInstrument.InstrumentCmds);

            waveformInstrument.InputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            //waveformInstrument.OutputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            waveformInstrument.InputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            //waveformInstrument.OutputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();

            triggerAtPowerOn(waveformInstrument.InstrumentCmds);

            #region Set a good Timescale
            double tuneTime = waveformInstrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points).GetTuneTimeWithPercentalLimits(130, 70);
            double scaling = tuneTime / 8;
            waveformInstrument.InstrumentCmds.SetTimebasePosition(4 * scaling);
            waveformInstrument.InstrumentCmds.SetTimebaseScale(scaling);
            #endregion

            triggerAtPowerOn(waveformInstrument.InstrumentCmds);

            maxInrushCurrent = waveformInstrument.InputCurrentDigitizerCmds.GetMaxVal();

            #region Set Power Analyzer Settings
            if (waveformInstrument.InstrumentCmds.GetType() == typeof(PACmds))
            {
                ((PADigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetHWRange(maxInrushCurrent);
                ((PADigitizerCmds)(waveformInstrument.InputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().InputVoltage);
            }
            #endregion

            #region Scale the waveform
            waveformInstrument.InputCurrentDigitizerCmds.SetVerticalScale(maxInrushCurrent / 6);
            scaling = waveformInstrument.InputCurrentDigitizerCmds.GetVerticalScale();
            waveformInstrument.InputCurrentDigitizerCmds.SetVerticalOffset(scaling * 3);
            waveformInstrument.InputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);
            #endregion

            triggerAtPowerOn(waveformInstrument.InstrumentCmds);

            maxInrushCurrent = waveformInstrument.InputCurrentDigitizerCmds.GetMaxVal();

            #region Fetch the waveform data from the power analyzer
            inputVoltageWave = waveformInstrument.InputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputVoltageWave = waveformInstrument.OutputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            inputCurrentWave = waveformInstrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputCurrentWave = waveformInstrument.OutputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            #endregion

            #region Process the results
            GetParent<Run_PS_Testplan>().Results.Publish("Peak inrush current (Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new { strMaxInrushCurrent = maxInrushCurrent });
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveforms at turn on\n(Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Input voltage (V)", "Output voltage (V)", "Input current (I)", "Output current (I)" }, inputVoltageWave.timeAxisValues.ToArray(), inputVoltageWave.yAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());
            #endregion
        }

        private void triggerAtPowerOn(IWaveformDisplayInstrCmds waveformInstrCmds)
        {
            powerSupply.SetVoltage(0);
            TestPlan.Sleep(1000);           //Wait a second
            waveformInstrCmds.SetSingle();
            TestPlan.Sleep(2000);           //Wait a second
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);
            TestPlan.Sleep(2000);           //Wait a second
        }
    }
}
