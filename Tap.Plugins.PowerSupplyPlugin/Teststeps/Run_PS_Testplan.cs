﻿#define debug

// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Keysight.Tap;
using System.Collections.Generic;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Agilent.CommandExpert.ScpiNet.AgInfiniiVision3000X_7_11;
using Agilent.CommandExpert.ScpiNet.Ag8990_1_2_6_1;
using static Tap.Plugins.PowerSupplyPlugin.Instruments.CustomSettings;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Agilent.CommandExpert.ScpiNet.AgAC6800_A_01_00_0067;
using System;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Run Power Supply Test Plan", Group: "Switching Mode Power Supplies", Description: "Insert a description here.")]
    public class Run_PS_Testplan : TestStep
    {
        #region Propertys which can be useful for every part of this class
        private List<MultiChanInstrument> allMultiChanInstruments
        {
            get
            {
                List<MultiChanInstrument> tempList = new List<MultiChanInstrument>();

                foreach (Instrument tempI in InstrumentSettings.Current)
                {
                    if (tempI is MultiChanInstrument)
                    {
                        tempList.Add((MultiChanInstrument)tempI);
                    }
                }
                return tempList;
            }
        }
        private List<SingleChanInstrument> allSingleChanInstruments
        {
            get
            {
                List<SingleChanInstrument> tempList = new List<SingleChanInstrument>();

                foreach (Instrument tempI in InstrumentSettings.Current)
                {
                    if (tempI is SingleChanInstrument)
                    {
                        tempList.Add((SingleChanInstrument)tempI);
                    }
                }
                return tempList;
            }
        }
        //Returns a List which includes an Instrument object for every channel that has oscilloscope functionality (may include the same object several times if it has several oscilloscope channels)
        private List<CustomInstrument> instrumentsWithOscilloscopeChannels
        {
            get
            {
                List<CustomInstrument> OscilloscopeList = new List<CustomInstrument>();

                //Iterate through the Multi Channel Instruments and save the instruments with an Oscilloscope into a list
                foreach (MultiChanInstrument IteratorI in allMultiChanInstruments)
                {
                    foreach (int Chan in IteratorI.Scope_Chans)
                    {
                        OscilloscopeList.Add(IteratorI);
                    }
                }
                //Iterate through the Single Channel Instruments and save the Oscilloscopes into a list
                foreach (SingleChanInstrument IteratorI in allSingleChanInstruments)
                {
                    if (IteratorI.getInstrumentTypeString() == "Oscilloscope")
                    {
                        OscilloscopeList.Add(IteratorI);
                    }
                }

                return OscilloscopeList;
            }
        }
        //Returns a List which includes an Instrument object for every channel that has Load functionality (may include the same object several times if it has several load channels)
        private List<CustomInstrument> instrumentsWithLoadChannels
        {
            get
            {
                List<CustomInstrument> LoadList = new List<CustomInstrument>();

                //Iterate through the Multi Channel Instruments and save the instruments with an Oscilloscope into a list
                foreach (MultiChanInstrument IteratorI in allMultiChanInstruments)
                {
                    foreach (int Chan in IteratorI.LoadChannels)
                    {
                        LoadList.Add(IteratorI);
                    }
                }
                //Iterate through the Single Channel Instruments and save the Oscilloscopes into a list
                foreach (SingleChanInstrument IteratorI in allSingleChanInstruments)
                {
                    if (IteratorI.getInstrumentTypeString() == "Electronic Load")
                    {
                        LoadList.Add(IteratorI);
                    }
                }

                return LoadList;
            }
        }
        //Returns a List which includes an Instrument object for every channel that has Power Analyzer functionality (may include the same object several times if it has several power analyzer channels)
        private List<MultiChanInstrument> instrumentsWithPowerAnalyzerChannels
        {
            get
            {
                List<MultiChanInstrument> PAList = new List<MultiChanInstrument>();

                //Iterate through the Multi Channel Instruments and save the instruments with an Oscilloscope into a list
                foreach (MultiChanInstrument IteratorI in allMultiChanInstruments)
                {
                    foreach (int Chan in IteratorI.PAChans2A)
                    {
                        PAList.Add(IteratorI);
                    }
                }

                return PAList;
            }
        }
        //Returns a List which includes an Instrument object for every channel that has Power Supply functionality (may include the same object several times if it has several power supply channels)
        private List<CustomInstrument> instrumentsWithPowerSupplyChannels
        {
            get
            {
                List<CustomInstrument> PowerSupplyList = new List<CustomInstrument>();

                //Iterate through the Multi Channel Instruments and save the instruments with an Oscilloscope into a list
                foreach (MultiChanInstrument IteratorI in allMultiChanInstruments)
                {
                    foreach (int Chan in IteratorI.AC_PS_Chans)
                    {
                        PowerSupplyList.Add(IteratorI);
                    }
                    foreach (int Chan in IteratorI.DC_PS_Chans)
                    {
                        PowerSupplyList.Add(IteratorI);
                    }
                }
                //Iterate through the Single Channel Instruments and save the Oscilloscopes into a list
                foreach (SingleChanInstrument IteratorI in allSingleChanInstruments)
                {
                    if (IteratorI.getInstrumentTypeString() == "AC Power Supply" || IteratorI.getInstrumentTypeString() == "DC Power Supply")
                    {
                        PowerSupplyList.Add(IteratorI);
                    }
                }

                return PowerSupplyList;
            }
        }

        public string GetInstrumentDescription(ScpiInstrument instr)
        {
            return instr.Name + " (" + instr.VisaAddress + ")";
        }

        private const string noInstrString = "No Instrument";
        public List<string> InstrumentsStringList
        {
            get
            {
                List<string> temp = new List<string>();

                temp.Add(noInstrString);

                foreach (ScpiInstrument scpiI in allMultiChanInstruments)
                {
                    temp.Add(GetInstrumentDescription(scpiI));
                }

                return temp;
            }
        }
        private List<int> allScopeChannels
        {
            get
            {
                return new List<int>() { ScopeInputVoltageChannelNb, ScopeInputCurrentChannelNb, ScopeOutputVoltageChannelNb, ScopeOutputCurrentChannelNb };
            }
        }
        #endregion

        //Interlock support
        #region isInterlockFeatureUsed
        [DisplayAttribute("Use the Interlock Feature", Group: "Interlock Feature", Description: "Check if the Output shall be disabled for a defined voltage at the Interlock pin of the Power Supply", Order: 1)]
        public bool isInterlockFeatureUsed { get; set; }
        #endregion

        #region inhibitPin
        public int inhibitPin = 3;
        #endregion

        #region nominal state of the inhibit pin
        public string nominalState = "Positive";
        #endregion

        //Waveform aquistion
        #region Number of points
        [DisplayAttribute("Maximum Number of Points", Group: "Aquired Waveforms", Description: "Specifies the maximum number of points which are aquired for one waveform which is going to be loaded into TAP. This reduces the speed for gathering and processing the waveform data.")]
        public int Points { get; set; }
        #endregion

        //DUT
        #region declare InputVoltage
        [DisplayAttribute("Input Voltage", Group: "Device Under Test", Description: "Specifies the input voltage of the DUT\n Input Voltage is actively set by the choosen Power Supply" +
            ".", Order: 1)]
        [Unit("V")]
        public double InputVoltage
        {
            get
            {
                return _input_Voltage;
            }
            set
            {
                _input_Voltage = value;
                _LimitInputPower = InputVoltage * LimitInputCurr;
            }
        }
        private double _input_Voltage;
        #endregion

        #region declare ExpectedDUTOutputVoltage
        [DisplayAttribute("Expected Output Voltage", Group: "Device Under Test", Description: "Specified output Voltage of the DUT\n Output Voltage is actively set if the DUT is SCPI capable.", Order: 2)]
        [Unit("V")]
        public double ExpectedDUTOutputVoltage
        {
            get
            {
                return _expected_DUT_output_Voltage;
            }
            set
            {
                _expected_DUT_output_Voltage = value;
                _maximum_output_power = _expected_DUT_output_Voltage * MaximumOutputCurrent;
            }
        }
        private double _expected_DUT_output_Voltage;
        #endregion

        #region declare MaximumOutputCurrent
        [DisplayAttribute("Maximum Output Current", Group: "Device Under Test", Description: "Maximum output current the DUT is rated for.\n Electronic load (if used) will limit the current according to this setting.", Order: 3)]
        [Unit("A")]
        public double MaximumOutputCurrent
        {
            get
            {
                return _maximum_output_current;
            }
            set
            {
                _maximum_output_current = value;
                _maximum_output_power = ExpectedDUTOutputVoltage * _maximum_output_current;
            }
        }
        double _maximum_output_current;
        #endregion

        #region declare MaximumOutputPower
        [Display("Maximum Output Power", Group: "Device Under Test", Description: "Maximum output power the DUT is rated for", Order: 4)]
        [Unit("W")]
        public double MaximumOutputPower
        {
            get
            {
                return _maximum_output_power;
            }
            set
            {
                _maximum_output_power = value;
                _maximum_output_current = _maximum_output_power / ExpectedDUTOutputVoltage;
            }
        }
        double _maximum_output_power;
        #endregion

        //Loadsettings
        #region declare LoadInstrument
        //Loadsettings
        [EnabledIf("IsResistorUsed", false, HideIfDisabled = true)]
        [Display("Instrument", Group: "Loadsettings", Description: "Instrument that simulates a load for the DUT")]
        public CustomInstrument LoadInstrument { get; set; }
        #endregion

        #region declaration LoadChannel
        [EnabledIf("ShowLoadChannel", HideIfDisabled = true)]
        [AvailableValues("LoadChannels")]
        [DisplayAttribute("Channel", Group: "Loadsettings", Description: "Channel of the instrument that will act as a loadsink", Order: 3)]
        public int LoadChannel
        {
            get
            {
                return LoadChannelCmds.channel;
            }
            set
            {
                LoadChannelCmds.channel = value;
            }
        }
        public bool ShowLoadChannel
        {
            get
            {
                if (LoadInstrument == null)
                {
                    return false;
                }
                else if (IsElectronicLoadUsed && LoadInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public List<int> LoadChannels
        {
            get
            {
                if (LoadInstrument == null)
                {
                    return null;
                }
                else if (LoadInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)LoadInstrument;

                    return multiChanInstr.LoadChannels;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region declare Resistance
        [EnabledIf("IsResistorUsed", true, HideIfDisabled = true)]
        [Unit("Ω")]
        [DisplayAttribute("Resistance", Group: "Loadsettings", Description: "Resistance of the Loadresistor used")]
        public double Resistance { get; set; }
        #endregion

        #region declare IsResistorUsed
        [Display("Use Resistor as Load", Group: "Loadsettings", Description: "Needs to be checked if a resistor is used as a load", Order: 5)]
        public bool IsResistorUsed
        {
            get { return _isResistorUsed; }
            set
            {
                _isResistorUsed = value;
                _isElectronicLoadUsed.ActualizeElectronicLoadCheckbox(_isResistorUsed);
            }
        }
        private bool _isResistorUsed;
        #endregion

        #region declare IsElectronicLoadUsed
        [Display("Use Electronic Load", Group: "Loadsettings", Description: "Needs to be checked if a electronic load is connected to the DUT", Order: 6)]
        public bool IsElectronicLoadUsed
        {
            get
            {
                return _isElectronicLoadUsed;
            }
            set
            {
                _isElectronicLoadUsed = value;
                _isResistorUsed.ActualizeResistorLoadCheckbox(_isElectronicLoadUsed);
            }
        }
        private bool _isElectronicLoadUsed;
        #endregion

        //Power Supply settings
        #region declare PowerSupplyInstrument

        [Display("Power Supply (AC or DC)", Group: "Power Supply Settings", Order: 1)]
        public CustomInstrument PowerSupplyInstrument { get; set; }
        #endregion

        #region declare PSChannel
        [EnabledIf("showPSChannelSelection", true, HideIfDisabled = true)]
        [AvailableValues("PS_Chans")]
        [DisplayAttribute("Channel", Group: "Power Supply Settings", Description: "Channel which supplies the DUT", Order: 2)]
        public int PSChannel { get; set; }

        public List<int> PS_Chans
        {
            get
            {
                if (PowerSupplyInstrument == null)
                {
                    return null;
                }
                else if (PowerSupplyInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)PowerSupplyInstrument;

                    return multiChanInstr.GetPowerSupplyChans();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool showPSChannelSelection
        {
            get
            {
                if (PowerSupplyInstrument == null)
                {
                    return false;
                }
                else if (PowerSupplyInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region declare LimitInputCurr
        [Display(Name: "Limit for Input Current", Description: "Determines the highest current the DUT is allowed to consume", Group: "Power Supply Settings", Order: 3)]
        [Unit("A")]
        public double LimitInputCurr
        {
            get
            {
                return _LimitInputCurr;
            }
            set
            {
                _LimitInputCurr = value;
                _LimitInputPower = _LimitInputCurr * InputVoltage;
            }
        }
        double _LimitInputCurr;
        #endregion

        #region declare MaxInputPower
        [Display(Name: "Limit for Input Power", Description: "Determines the highest power the DUT is allowed to consume", Group: "Power Supply Settings", Order: 4)]
        [Unit("W")]
        public double LimitOutputPower
        {
            get
            {
                return _LimitInputPower;
            }
            set
            {
                _LimitInputPower = value;
                _LimitInputCurr = value / InputVoltage;
            }
        }
        private double _LimitInputPower;
        #endregion

        //Input power analysis
        public List<int> Scope_Input_Chans
        {
            get
            {
                if (ScopeInputInstrument == null)
                {
                    return null;
                }
                else if (ScopeInputInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)ScopeInputInstrument;

                    return multiChanInstr.GetScopeChans();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool IsScopeInputSelectionShown
        {
            get
            {
                if (ScopeInputString == "No Instrument")
                {
                    return false;
                }
                else if (ScopeInputInstrument != null && ScopeInputInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region declare PowerAnalyzerInput
        [AvailableValues("InstrumentsStringList")]
        [Display("Power Analyzer", Group: "Inputpower Analysis", Order: 1)]
        public string PAInputString
        {
            get
            {
                return _PAInputString;
            }
            set
            {
                _PAInputString = value;

                //Sets the scopeInput
                foreach (MultiChanInstrument counterI in allMultiChanInstruments)
                {
                    if (GetInstrumentDescription(counterI) == PAInputString)
                    {
                        PowerAnalyzerInput = counterI;
                    }
                }

                //Presets the Scope Channels to values which make sence
                try
                {
                    if (PA_InputChannel == -1)
                    {
                        PA_InputChannel = PowerAnalyzerInput.GetPowerAnalyzerChans()[0];
                    }
                }
                catch
                {
                    //Do nothing
                }
            }
        }

        string _PAInputString;

        private MultiChanInstrument PowerAnalyzerInput;
        #endregion

        #region declare PA_InputChannel
        [EnabledIf("IsPA_InputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("PAChans2A")]
        [DisplayAttribute("Channel PA", Group: "Inputpower Analysis", Description: "Channel of the Power Analyzer which observes input voltage and intput current", Order: 2)]
        public int PA_InputChannel { get; set; }
        public List<int> PAChans2A
        {
            get
            {
                if (PowerAnalyzerInput == null)
                {
                    return null;
                }
                else if (PowerAnalyzerInput.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)PowerAnalyzerInput;

                    return multiChanInstr.GetPowerAnalyzerChans();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool IsPA_InputSelectionShown
        {
            get
            {
                if (PAInputString == noInstrString)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion

        #region declare OscilloscopeInputInstrument
        //Used to select the instrument which acts as an oscilloscope that observes the input

        [AvailableValues("InstrumentsStringList")]
        [Display("Oscilloscope", Description: "An oscilloscope is needed to analyze voltages and currents with a high bandwidth. Select an instrument that has at least two channels for power analysis.", Group: "Inputpower Analysis", Order: 3)]
        public string ScopeInputString
        {
            get
            {
                return _Scope_Input_string;
            }
            set
            {
                _Scope_Input_string = value;

                //Sets the scopeInput
                foreach (MultiChanInstrument counterI in allMultiChanInstruments)
                {
                    if (GetInstrumentDescription(counterI) == ScopeInputString)
                    {
                        ScopeInputInstrument = counterI;
                    }
                }

                //Presets the Scope Channels to values which make sence
                try
                {
                    if (ScopeInputCurrentChannelNb == -1)
                    {
                        ScopeInputCurrentChannelNb = ScopeInputInstrument.Scope_Chans[0];
                        ScopeInputVoltageChannelNb = ScopeInputInstrument.Scope_Chans[1];
                    }
                }
                catch
                {
                    //Do nothing
                }
            }
        }
        private string _Scope_Input_string;

        public MultiChanInstrument ScopeInputInstrument;
      
        #endregion

        #region declare OscilloscopeVoltageChannel
        //Used to select the oscilloscope channel which observes the input voltage of the DUT
        //Disabled and hidden if no input oscilloscope is selected

        [EnabledIf("IsScopeInputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("Scope_Input_Chans")]
        [DisplayAttribute("Voltage Channel Scope", Group: "Inputpower Analysis", Description: "Channel of the Oscilloscope", Order: 4)]
        public int ScopeInputVoltageChannelNb { get; set; }
        #endregion

        #region declare OscilloscopeCurrentChannel
        //Used to select the oscilloscope channel which observes the input current of the DUT
        //Disabled and hidden if no input oscilloscope is selected

        [EnabledIf("IsScopeInputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("Scope_Input_Chans")]
        [DisplayAttribute("Current Channel Scope", Group: "Inputpower Analysis", Description: "Channel of the Oscilloscope", Order: 4)]
        public int ScopeInputCurrentChannelNb { get; set; }

        #endregion

        //Output power Analysis
        public List<int> ScopeOutputChans
        {
            get
            {
                if (ScopeOutputInstrument == null)
                {
                    return null;
                }
                else if (ScopeOutputInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)ScopeOutputInstrument;

                    return multiChanInstr.GetScopeChans();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool IsScopeOutputSelectionShown
        {
            get
            {
                if (ScopeOutputString == "No Instrument")        //No Scope selcted. Hide Channel Selection.
                {
                    return false;
                }
                else if (ScopeOutputInstrument != null && ScopeOutputInstrument.GetType() == typeof(MultiChanInstrument))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region declare PowerAnalyzerOutput
        [AvailableValues("InstrumentsStringList")]
        [Display("Power Analyzer", Group: "Outputpower Analysis", Order: 1)]
        public string PAOutputString
        {
            get
            {
                return _PAOutputString;
            }
            set
            {
                _PAOutputString = value;

                //Sets the scopeInput
                foreach (MultiChanInstrument counterI in allMultiChanInstruments)
                {
                    if (GetInstrumentDescription(counterI) == PAOutputString)
                    {
                        PowerAnalyzerOutput = counterI;
                    }
                }

                //Presets the Power Analyzer Channels to values which make sence
                try
                {
                    if (PA_OutputChannel == -1)
                    {
                        PA_OutputChannel = PowerAnalyzerOutput.GetPowerAnalyzerChans()[1];
                    }
                }
                catch
                {
                    //Do nothing
                }
            }
        }

        public string _PAOutputString;

        private MultiChanInstrument PowerAnalyzerOutput;
        #endregion

        #region declare PA_OutputChannel
        [EnabledIf("IsPA_OutputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("PA_OutputChans")]
        [DisplayAttribute("Channel", Group: "Outputpower Analysis", Description: "Channel of the Power Analyzer which observes output voltage and output current", Order: 2)]
        public int PA_OutputChannel { get; set; }
        public List<int> PA_OutputChans
        {
            get
            {
                if (PowerAnalyzerOutput == null)
                {
                    return null;
                }
                else if (PowerAnalyzerOutput.GetType() == typeof(MultiChanInstrument))
                {
                    MultiChanInstrument multiChanInstr = (MultiChanInstrument)PowerAnalyzerOutput;

                    return multiChanInstr.GetPowerAnalyzerChans();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool IsPA_OutputSelectionShown
        {
            get
            {
                if (PAOutputString == noInstrString)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion

        #region declare OscilloscopeOutputInstrument
        //Used to select the instrument which acts as an oscilloscope that observes the output

        [AvailableValues("InstrumentsStringList")]
        [Display("Oscilloscope", Description: "An oscilloscope is needed to analyze voltages and currents with a high bandwidth. Select an instrument that has at least two channels for power analysis.", Group: "Outputpower Analysis", Order: 3)]
        public string ScopeOutputString
        {
            get
            {
                return _scopeOutputString;
            }
            set
            {
                _scopeOutputString = value;

                foreach (MultiChanInstrument counterI in allMultiChanInstruments)
                {
                    if (GetInstrumentDescription(counterI) == _scopeOutputString)
                    {
                        ScopeOutputInstrument = counterI;
                    }
                }

                
                //Presets the Scope Channels to values which make sence
                try
                {
                    if (ScopeOutputCurrentChannelNb == -1)
                    {
                        ScopeOutputCurrentChannelNb = ScopeOutputInstrument.Scope_Chans[2];
                        ScopeOutputVoltageChannelNb = ScopeOutputInstrument.Scope_Chans[3];
                    }
                }
                catch
                {
                    //Do nothing
                }
            }
        }
        private string _scopeOutputString;

        public MultiChanInstrument ScopeOutputInstrument;
        #endregion

        #region declare OscilloscopeVoltageChannel
        //Used to select the oscilloscope channel which observes the output voltage of the DUT
        //Disabled and hidden if no input oscilloscope is selected

        [EnabledIf("IsScopeOutputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("ScopeOutputChans")]
        [DisplayAttribute("Voltage Channel Scope", Group: "Outputpower Analysis", Description: "Channel of the Oscilloscope", Order: 4)]
        public int ScopeOutputVoltageChannelNb { get; set; }
        #endregion

        #region declare OscilloscopeCurrentChannel
        //Used to select the oscilloscope channel which observes the output current of the DUT
        //Disabled and hidden if no input oscilloscope is selected

        [EnabledIf("IsScopeOutputSelectionShown", HideIfDisabled = true)]
        [AvailableValues("ScopeOutputChans")]
        [DisplayAttribute("Current Channel Scope", Group: "Outputpower Analysis", Description: "Channel of the Oscilloscope", Order: 4)]
        public int ScopeOutputCurrentChannelNb { get; set; }
        #endregion


        //Instrument channel declarations
        public ElLoadChanCmds LoadChannelCmds;
        public PSChanCmds PSChannelCmds;

        public PSPluginPowerAnalyzer powerAnalyzer;
        public PSPluginScope scope;

        public PSPluginWaveformInstrumentAdaptor powerAnalyzerAdaptor;
        public PSPluginWaveformInstrumentAdaptor scopeAdaptor;

        public Run_PS_Testplan()
        {
            //Default Values for Debugging
#if debug
            InputVoltage = 16;
            ExpectedDUTOutputVoltage = 5;
            LimitInputCurr = 1;
            MaximumOutputCurrent = 0.5;
#endif

            Points = 2000;

            ScopeOutputCurrentChannelNb = -1;
            ScopeOutputVoltageChannelNb = -1;
            ScopeInputCurrentChannelNb = -1;
            ScopeInputVoltageChannelNb = -1;

            #region Default values for the load
            LoadChannelCmds = new ElLoadChanCmds(null, -1);

            try
            {
                LoadInstrument = instrumentsWithLoadChannels[0];
            }
            catch
            {
                //do nothing (field will stay empty)
            }

            try
            {
                LoadChannel = ((MultiChanInstrument)LoadInstrument).LoadChannels[0];          //Set a default Load Channel
            }
            catch
            {
                //do nothing
            }
            #endregion

            // ToDo: Set default values for properties / settings.
            _isResistorUsed = false;
            _isElectronicLoadUsed= true;

            #region Default values for the power analysis

            PA_OutputChannel = -1;
            PA_InputChannel = -1;

            PAInputString = noInstrString;
            PAOutputString = noInstrString;

            #endregion

            #region Default Oscilloscope settings
            ScopeOutputString = noInstrString;
            ScopeInputString = noInstrString;
            #endregion

            #region Default values for the power supply settings
            PowerSupplyInstrument = instrumentsWithPowerSupplyChannels[0];
            try
            {
                PSChannel = ((MultiChanInstrument)PowerSupplyInstrument).GetPowerSupplyChans()[0];          //Set a default Power Supply Channel
            }
            catch
            {
                //do nothing
            }
            #endregion

            #region rule validation
            Rules.Add(checkResistance, "Format not supported", "Resistance");
            Rules.Add(checkLoadChannel, "Channel Number not valid", "loadChannel");

            Rules.Add(checkDoubledPAChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "PA_InputChannel");
            Rules.Add(checkDoubledPAChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "PA_OutputChannel");
            Rules.Add(checkDoubledOscInpVoltChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "ScopeInputVoltageChannelNb");
            Rules.Add(checkDoubledOscInpCurrChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "ScopeInputCurrentChannelNb");
            Rules.Add(checkDoubledOscOutpVoltChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "ScopeOutputVoltageChannelNb");
            Rules.Add(checkDoubledOscOutpCurrChannels, "One channel of the same instrument can not be connected to the input and output of the DUT at the same time.", "ScopeOutputCurrentChannelNb");
            #endregion
        }

        public override void PrePlanRun()
        {
            try
            {
                //Initialize the Set Instrument Object
                LoadChannelCmds = new ElLoadChanCmds(new ELoadCmds(LoadInstrument), LoadChannel);

                //Initialize the PS_Channel object
                
                PSChannelCmds = PowerSupplyInstrument.GetNewPSChannelCmds(PSChannel);
            }
            catch
            {
                Log.Error("Not all the instruments are properly set. Select an instrument for each channel and try running the code again.");
                UpgradeVerdict(Verdict.Aborted);
                OfferBreak();
            }

            try
            {
                PowerAnalyzerInput.InitializeInstrument(PowerAnalyzerInput.VisaAddress);
                powerAnalyzer = new PSPluginPowerAnalyzer(PowerAnalyzerInput, PA_InputChannel, PA_OutputChannel);

                //Initialization of the power analyzer adaptor
                powerAnalyzerAdaptor.InputCurrentDigitizerCmds = powerAnalyzer.InputCmds.CURRentDigitizer;
                powerAnalyzerAdaptor.InputVoltageDigitizerCmds = powerAnalyzer.InputCmds.VOLTageDigitizer;
                powerAnalyzerAdaptor.OutputCurrentDigitizerCmds = powerAnalyzer.OutputCmds.CURRentDigitizer;
                powerAnalyzerAdaptor.OutputVoltageDigitizerCmds = powerAnalyzer.OutputCmds.VOLTageDigitizer;
                powerAnalyzerAdaptor.InstrumentCmds = powerAnalyzer.InstrumentCmds;

                ((PACmds)(powerAnalyzer.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM3);

                //Set timeouts to make slow measurements like noise measuring possible
                PowerAnalyzerOutput.IoTimeout = 10000;
                PowerAnalyzerInput.IoTimeout = 10000;
            }
            catch
            {
                //Nothing happens when no PowerAnalyzer is selected
            }


            //Initialize Oscilloscope(s)
            try
            {
                ScopeInputInstrument.InitializeInstrument(ScopeInputInstrument.VisaAddress);
                scope = new PSPluginScope(ScopeInputInstrument, ScopeInputVoltageChannelNb, ScopeOutputVoltageChannelNb, ScopeInputCurrentChannelNb, ScopeOutputCurrentChannelNb);

                scopeAdaptor.InputCurrentDigitizerCmds = scope.InputCurrentCmds;
                scopeAdaptor.InputVoltageDigitizerCmds = scope.InputVoltageCmds;
                scopeAdaptor.OutputCurrentDigitizerCmds = scope.OutputCurrentCmds;
                scopeAdaptor.OutputVoltageDigitizerCmds = scope.OutputVoltageCmds;
                scopeAdaptor.InstrumentCmds = scope.InstrumentCmds;
            }
            catch
            {
                //Nothing happens when no Oscilloscopes are selected
            }

            if (isInterlockFeatureUsed)
            {
                PSChannelCmds.InstrumentCommands.SetDigitalPinFunction(this.inhibitPin,PSCmds.function.INHibit);

                if (this.nominalState == "POSitive")
                {
                    PSChannelCmds.InstrumentCommands.SetDigitalPinPolarity(this.inhibitPin, PSCmds.polarity.POSitive);
                }
                else if (this.nominalState == "NEGative")
                {
                    PSChannelCmds.InstrumentCommands.SetDigitalPinPolarity(this.inhibitPin,PSCmds.polarity.NEGative);
                }
                PSChannelCmds.InstrumentCommands.SetInhibitMode(PSCmds.mode.LIVE);
            }
            else if(isInterlockFeatureUsed && PowerSupplyInstrument.GetType() == typeof(AgN6705))
            {
                PSChannelCmds.InstrumentCommands.SetInhibitMode(PSCmds.mode.OFF);
            }

            base.PrePlanRun();
        }

        public override void Run()
        {
            RunChildSteps();
        }

        public void ResetAllInstruments()
        {
            #region Reset all the instruments to put them into a save state
            LoadInstrument.Reset();
            if (LoadInstrument != PowerSupplyInstrument)
            {
                PowerSupplyInstrument.Reset();
            }

            if (PowerAnalyzerInput != null && PowerAnalyzerOutput != null)
            {
                PowerAnalyzerOutput.Reset();
                if (PowerAnalyzerOutput != PowerAnalyzerInput)
                {
                    PowerAnalyzerInput.Reset();
                }

                powerAnalyzer.InputCmds.CURRentDigitizer.SetCurrentInput();
                powerAnalyzer.OutputCmds.CURRentDigitizer.SetCurrentInput();
            }

            if (ScopeInputInstrument != null && ScopeOutputInstrument != null)
            {
                ScopeInputInstrument.Reset();
            }
            #endregion

            //Set the maximum output current for the Power Supply to protect the DUT
            PSChannelCmds.SetCurrentLimit(LimitInputCurr);
        }

        public override void PostPlanRun()
        {
            //Reset all the instruments to put them into a save state
            //Will not run while debugging
            #if debug
            #else
            DUT.Reset();
            DUT_load_Instrument.Reset();
            powerSupplyInstrument.Reset();
            PowerAnalyzerOutput.Reset();
            PowerAnalyzerInput.Reset();
            Oscilloscope.Reset();
            #endif

            UpgradeVerdict(Verdict.Pass);

            base.PostPlanRun();
        }

        //Checks if the value entered in the Resistance field is valid
        private bool checkResistance()
        {
            if (!IsElectronicLoadUsed)
            {

            }
            return true;
        }

        private bool checkLoadChannel()
        {
            if (LoadChannel >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool checkDoubledPAChannels()
        {
            if (PowerAnalyzerInput == PowerAnalyzerOutput && PA_InputChannel != -1)
            {
                if (PA_InputChannel == PA_OutputChannel)
                {
                    return false;
                }
            }

            return true;
        }

        private bool checkDoubledOscInpVoltChannels()
        {
            int counter = 0;

            if(ScopeInputString == "No Instrument")
            {
                return true;            //No error message is thrown when No Instrument is selected
            }

            foreach (int i in allScopeChannels)
            {
                if (i == ScopeInputVoltageChannelNb)
                {
                    counter = counter + 1;
                }
            }
            if (counter > 1)
            {
                return false;
            }

            return true;
        }

        private bool checkDoubledOscInpCurrChannels()
        {
            int counter = 0;

            if (ScopeInputString == "No Instrument")
            {
                return true;            //No error message is thrown when No Instrument is selected
            }

            foreach (int i in allScopeChannels)
            {
                if (i == ScopeInputCurrentChannelNb)
                {
                    counter = counter + 1;
                }
            }
            if (counter > 1)
            {
                return false;
            }

            return true;
        }

        private bool checkDoubledOscOutpVoltChannels()
        {
            int counter = 0;

            if (ScopeOutputString == "No Instrument")
            {
                return true;            //No error message is thrown when No Instrument is selected
            }

            foreach (int i in allScopeChannels)
            {
                if (i == ScopeOutputVoltageChannelNb)
                {
                    counter = counter + 1;
                }
            }
            if (counter > 1)
            {
                return false;
            }

            return true;
        }

        private bool checkDoubledOscOutpCurrChannels()
        {
            int counter = 0;

            if (ScopeOutputString == "No Instrument")
            {
                return true;            //No error message is thrown when No Instrument is selected
            }

            foreach (int i in allScopeChannels)
            {
                if (i == ScopeOutputCurrentChannelNb)
                {
                    counter = counter + 1;
                }
            }
            if (counter > 1)
            {
                return false;
            }

            return true;
        }
    }
}