﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;


namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Efficiency", Group: "Switching Mode Power Supplies", Description: "Measures the efficiency of the DUT in a specific mode.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class Efficiency : TestStep
    {
        #region declaration Delaytime
        [EnabledIf("IsSCPI_DUT_used", true, HideIfDisabled = true)]
        [Unit("ms")]
        [DisplayAttribute(Name: "Delay time", Description: "Time between appliing the output current and measuring the efficiency", Group: "Settings", Order: 2)]
        public int Delaytime { get; set; }
        #endregion

        //Instruments needed
        private PSChanCmds powerSupply;
        private ElLoadChanCmds electronicLoad;

        private PSPluginPowerAnalyzer powerAnalyzer;

        public Efficiency()
        {
            // ToDo: Set default values for properties / settings.

            //Rule Validation (example)
        }

        public override void PrePlanRun()
        {
            //Instruments are defined by Parentstep
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzer;

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            //Power the DUT
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            //Appy electronic load for predefined input current
            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            TestPlan.Sleep(1000);           //Wait a second for the DUT to power up

            if(powerSupply.GetType() == typeof(ACPSChanCmds))
            {
                measure((IACPowerMeasurement)powerSupply, (IDCPowerMeasurement)electronicLoad, powerSupply.InstrumentCommands.instrument, electronicLoad.InstrumentCommands.instrument);

                if (powerAnalyzer != null)
                {
                    measure((IACPowerMeasurement)powerAnalyzer.InputCmds, (IDCPowerMeasurement)powerAnalyzer.OutputCmds, powerAnalyzer.InstrumentCmds.instrument);
                }
            }
            else if(powerSupply.GetType() == typeof(DCPSChanCmds))
            {
                measure((IDCPowerMeasurement)powerSupply, (IDCPowerMeasurement)electronicLoad, powerSupply.InstrumentCommands.instrument, electronicLoad.InstrumentCommands.instrument);

                if (powerAnalyzer != null)
                {
                    measure((IDCPowerMeasurement)powerAnalyzer.InputCmds, (IDCPowerMeasurement)powerAnalyzer.OutputCmds, powerAnalyzer.InstrumentCmds.instrument);
                }
            }
            else
            {
                throw new Exception();
            }
            
            // ToDo: Add test case code.
            UpgradeVerdict(Verdict.Pass);
        }

        private void measure(IDCPowerMeasurement inputPowerDigitizerCmds, IDCPowerMeasurement outputPowerDigitizerCmds, CustomInstrument instrument, CustomInstrument instrument2 = null)
        {
            //Measure the source current of the AC/DC Power Supply with Power 
            double inputpower = inputPowerDigitizerCmds.PerformDCPowerMeasurementRoutine();
            double outputpower = outputPowerDigitizerCmds.PerformDCPowerMeasurementRoutine();

            ProcessMeasurement(inputpower, outputpower, instrument, instrument2);
        }

        private void measure(IACPowerMeasurement inputPowerDigitizerCmds, IDCPowerMeasurement outputPowerDigitizerCmds, CustomInstrument instrument, CustomInstrument instrument2 = null)
        {
            //Measure the source current of the AC/DC Power Supply with Power 
            double inputpower = inputPowerDigitizerCmds.PerformACPowerMeasurementRoutine();
            double outputpower = outputPowerDigitizerCmds.PerformDCPowerMeasurementRoutine();

            ProcessMeasurement(inputpower, outputpower, instrument, instrument2);
        }

        private void ProcessMeasurement(double inputpower, double outputpower, CustomInstrument instrument, CustomInstrument instrument2 = null)
        {
            double efficiency = outputpower / inputpower;

            string instrumentstr = "";
            if (instrument2 != instrument && instrument2 != null)
            {
                instrumentstr = instrument.Name + ", " + instrument2.Name;
            }
            else
            {
                instrumentstr = instrument.Name;
            }

            GetParent<Run_PS_Testplan>().Results.Publish("Efficiency (Instruments: " + instrumentstr + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() + ")", new { strEfficiency = efficiency });
            
        }
    }
}
