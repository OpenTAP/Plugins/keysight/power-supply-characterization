﻿//#define debug

// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Total Harmonic Distortion", Group: "Switching Mode Power Supplies", Description: "Measures the total harmonic distortion of the source current.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class TotalHarmonicDistortion : TestStep
    {
        private string strTHD;

        //[Display("Fundamental")]
        private double frequency { get; set; }
        private double THD;

        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;
        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;


        public TotalHarmonicDistortion()
        {
            // ToDo: Set default values for properties / settings.
        }
        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;

            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;

            base.PrePlanRun();
        }
        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);

            if (powerAnalyzer.InstrumentCmds != null)
            {
                measure(powerAnalyzer);
            }
            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }

            // ToDo: Add test case code.
            UpgradeVerdict(Verdict.Pass);
        }

        public void measure(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            //Power the DUT
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            waveformInstrument.InputVoltageDigitizerCmds.Autoscale();

            if ( waveformInstrument.InstrumentCmds.GetType() == typeof(ScopeCmds) )
            {
                ((ScopeDigitizerCmds)(waveformInstrument.InputVoltageDigitizerCmds)).SetPowerApplicationVoltage();
                ((ScopeDigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetPowerApplicationCurrent();
            }

            waveformInstrument.InputCurrentDigitizerCmds.ViewSource();
            waveformInstrument.InputVoltageDigitizerCmds.ViewSource();

            waveformInstrument.InputVoltageDigitizerCmds.RangeToFullWaveform();
            waveformInstrument.InputCurrentDigitizerCmds.RangeToFullWaveform();

            //Das Scope hat noch das Problem, dass die Grundfrequenz nicht richtig bestimmt wird, wenn die Stromwelle auf den vollen Bildschirm skaliert ist.
            THD = waveformInstrument.InputVoltageDigitizerCmds.PerformHarmonicsAnalysis();

            GetParent<Run_PS_Testplan>().Results.Publish("Total Harmonic Distortion (Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new { strTHD = THD });
        }
    }
}
