﻿// Author:      Jan Volpp (Jan.Volpp@k eysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Periodic and Random Deviation", Group: "Switching Mode Power Supplies", Description: "Measures ripple and noise performance.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class PeriodicAndRandomDeviation : TestStep
    {
        private double noise;

        private string strLFNoise = "Standard Deviation";
        private string strSWNoise = "Switching Noise";
        private string strTotalNoise = "Total Measurable Noise";

        [AvailableValues("BandSelection")]
        [DisplayAttribute("Band", Description: "Specifies the band in which periodic and random variations are measured")]
        public string Band { get; set; }
        public List<string> BandSelection
        {
            get
            {
                return new List<string> { strLFNoise, /*strSWNoise,*/ strTotalNoise};
            }
        }

        //Instruments needed
        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;

        public PeriodicAndRandomDeviation()
        {
            // Set default values for properties / settings.
            noise = -99;
            Band = strTotalNoise;
        }

        public override void PrePlanRun()
        {
            //Instruments are defined by Parentstep
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;

            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);

            if (powerAnalyzer.InstrumentCmds != null)
            {
                measure(powerAnalyzer);
            }
            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }

            UpgradeVerdict(Verdict.Pass);
        }

        public void measure(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            //Power the DUT
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage, CustomSettings.RelaySelection.NORelay);
            TestPlan.Sleep(2000);
            //powerSupply.SetSenseSource(CustomSettings.SenseSelection.External);


            //Adjust Power Analyzer Settings
            if(waveformInstrument.InstrumentCmds.GetType() == typeof(PACmds))
            { 
                ((PADigitizerCmds)(waveformInstrument.InputVoltageDigitizerCmds)).BlankSource();
                ((PACmds)(waveformInstrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM6);
            }

            waveformInstrument.OutputVoltageDigitizerCmds.ViewSource();
            waveformInstrument.OutputVoltageDigitizerCmds.SetCoupling(CustomSettings.CouplingSelection.AC);
            waveformInstrument.OutputVoltageDigitizerCmds.SetAntiAliasing(false);


            if (Band == strTotalNoise)
            {
                noise = waveformInstrument.OutputVoltageDigitizerCmds.WaveformCmds.GetNoise();
            }
            else if (Band == strSWNoise)
            {
                Log.Error("Teststep not yet implemented for specified settings");
            }
            else if (Band == strLFNoise)
            {
                noise = waveformInstrument.OutputVoltageDigitizerCmds.PerformStandardDeviationMeaurement();
            }

            GetParent<Run_PS_Testplan>().Results.Publish("Periodic And Random Deviation (Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new { strTotalNoise = noise });
        }
    }
}
