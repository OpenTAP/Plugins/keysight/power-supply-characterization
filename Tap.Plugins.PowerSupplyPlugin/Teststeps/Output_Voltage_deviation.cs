﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Output Voltage Deviation", Group: "Switching Mode Power Supplies", Description: "Determines the deviation between expected and real output voltage of the DUT.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class Output_Voltage_deviation : TestStep
    {
        string strDeviationPercent;

        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;

        public Output_Voltage_deviation()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;
            powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;

            base.PrePlanRun();
        }

        public override void Run()
        {
             GetParent<Run_PS_Testplan>().ResetAllInstruments();

            //Power the DUT
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            //Appy electronic load for predefined input current
            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            TestPlan.Sleep(2000);           //Wait a second for the DUT to power up

            measure(electronicLoad.InstrumentCommands.instrument,electronicLoad.VoltageMultimeter);

            if (powerAnalyzer.InstrumentCmds != null)
            {
                ((PADigitizerCmds)(powerAnalyzer.OutputVoltageDigitizerCmds)).SetAutorange(false);

                measure(powerAnalyzer.InstrumentCmds.instrument, powerAnalyzer.OutputVoltageDigitizerCmds);
            }
            UpgradeVerdict(Verdict.Pass);
        }

        private void measure(CustomInstrument instrument,IMultimeterDigitizerCmds multimeterDigitizerCmds)
        {
            double outputvoltage = multimeterDigitizerCmds.GetRmsValue();

            double deviation = (outputvoltage / GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage) - 1;
            double deviationPercent = deviation * 100;

            GetParent<Run_PS_Testplan>().Results.Publish("Output voltage deviation in percent (Instrument: " + instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() + ")", new { strDeviationPercent = deviationPercent });
        }
    }
}
