﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using static Tap.Plugins.PowerSupplyPlugin.Instruments.CustomSettings;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Load Effect (Load Regulation)", Group: "Switching Mode Power Supplies", Description: "Changes the load of the device under test and measures the affect on its output voltage.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class Load_effect : TestStep
    {
        private double scaling;
        private double riseTime;

        //Results to publish
        double lowLoadVoltage;
        double highLoadVoltage;
        double percentageVoltageDrop;

        string strpercentageVoltageDrop;
        private waveform inputVoltageWave;
        private waveform outputVoltageWave;
        private waveform inputCurrentWave;
        private waveform outputCurrentWave;

        #region declare loadchangeselection
        [Display("Suggestions for Loadchanges:", Description: "The load regulation for the selectable loadchanges must be specified according to chapter 3.7 of IEC61204", Order: 1)]
        [AvailableValues("loadchangesuggestions")]
        public string loadchangselection
        {
            get
            {
                return _loadchangeselection;
            }
            set
            {
                _loadchangeselection = value;
                if(_loadchangeselection == "0 to 100 %")
                {
                    _percentageLowLoadcurrent = 0;
                    _percentageHighLoadcurrent = 100;
                }
                else if(_loadchangeselection == "10 to 100 %")
                {
                    _percentageLowLoadcurrent = 10;
                    _percentageHighLoadcurrent = 100;
                }
                else if(_loadchangeselection == "25 to 100 %")
                {
                    _percentageLowLoadcurrent = 25;
                    _percentageHighLoadcurrent = 100;
                }
                else if(_loadchangeselection == "50 to 100 %")
                {
                    _percentageLowLoadcurrent = 50;
                    _percentageHighLoadcurrent = 100;
                }
                else if(_loadchangeselection == "custom Loadchange")
                {
                    //Do nothing
                }
                else
                {
                    Log.Error("Error. Invalid Textstring.");
                }
            }
        }

        private string _loadchangeselection;
        public List<string> loadchangesuggestions
        {
            get
            {
                return new List<string> { "custom Loadchange", "0 to 100 %", "10 to 100 %", "25 to 100 %", "50 to 100 %" };
            }
        }
        #endregion

        #region declare percentageLowLoadcurrent
        [Display("Low loadcurrent", Description: "In relation to the highest supported outputcurrent of the DUT", Order: 2)]
        [Unit("% of maximum DUT load")]
        public double percentageLowLoadcurrent
        {
            get
            {
                return _percentageLowLoadcurrent;
            }
            set
            {
                _percentageLowLoadcurrent = value;
                _loadchangeselection = "custom Loadchange";
            }
        }
        private double _percentageLowLoadcurrent { get; set; }
        #endregion

        #region declare percentageHighLoadcurrent
        [Display("High Load Current", Description: "In relation to the highest supported outputcurrent of the DUT", Order: 3)]
        [Unit("% of maximum DUT Load")]
        public double percentageHighLoadcurrent
        {
            get
            {
                return _percentageHighLoadcurrent;
            }
            set
            {
                _percentageHighLoadcurrent = value;
                _loadchangeselection = "custom Loadchange";
            }
        }
        private double _percentageHighLoadcurrent { get; set; }
        #endregion

        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;

        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;

        public Load_effect()
        {
            _loadchangeselection = "0 to 100 %";
        }

        private double highLoadcurrent, lowLoadcurrent;

        public override void PrePlanRun()
        {
            

            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;
            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;

            //Define Loadcurrents
            highLoadcurrent = GetParent<Run_PS_Testplan>().MaximumOutputCurrent * (percentageHighLoadcurrent / 100);
            lowLoadcurrent = GetParent<Run_PS_Testplan>().MaximumOutputCurrent * (percentageLowLoadcurrent / 100);

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);

            measureWithPowerConverter();

            if (powerAnalyzer.InstrumentCmds != null)
            {
                measure(powerAnalyzer);
            }
            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }
            UpgradeVerdict(Verdict.Pass);
        }

        //Measure the load effect just with the power Supply and the electronic Load
        public void measureWithPowerConverter()
        {
            //Power the DUT
            
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(lowLoadcurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
            TestPlan.Sleep(1000);           //Wait a second for the DUT to power up

            lowLoadVoltage = electronicLoad.GetRmsVoltage();


            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(highLoadcurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
            TestPlan.Sleep(1000);           //Wait a second for the DUT to power up

            highLoadVoltage = electronicLoad.GetRmsVoltage();

            //Calculation
            percentageVoltageDrop = -((highLoadVoltage / lowLoadVoltage) - 1) * 100;

            GetParent<Run_PS_Testplan>().Results.Publish("Voltage drop in percent(Instrument: " + electronicLoad.InstrumentCommands.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new { strpercentageVoltageDrop = percentageVoltageDrop });
        }

        //load effect measured with the power analyzer
        private void measure(PSPluginWaveformInstrumentAdaptor instrument)
        {
            scaling = 0.1;         //Set scaling to start with
            riseTime = -1;

            //Power the DUT
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            #region Set the preferences of the Waveform instrument

            //This part is only executed if the insrument is a Power Analyzer
            if (instrument.InstrumentCmds.GetType() == typeof(PACmds))
            {
                //Adjust Power Analyzer Settings
                ((PACmds)(instrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM3);

                //Set a high range to avoid error messages in cause of exeeding the range.
                ((PADigitizerCmds)(instrument.InputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().InputVoltage);
                ((PADigitizerCmds)(instrument.OutputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
                ((PADigitizerCmds)(instrument.InputCurrentDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
                ((PADigitizerCmds)(instrument.OutputCurrentDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().MaximumOutputCurrent*10);
            }

            //Set other preferences for the instrument
            instrument.OutputCurrentDigitizerCmds.SetVerticalOffset(0);
            instrument.OutputCurrentDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().MaximumOutputCurrent / 3);
            instrument.OutputCurrentDigitizerCmds.SetTriggerSource();
            instrument.InstrumentCmds.SetTriggerType(CustomSettings.TriggerType.EDGE);
            instrument.InstrumentCmds.SetSlope(CustomSettings.SlopeSelection.POSitive);
            double triggerLevel = (highLoadcurrent - lowLoadcurrent) / 2 + lowLoadcurrent;
            instrument.OutputCurrentDigitizerCmds.SetTriggerLevel(triggerLevel);
            //Set display preferences of the instrument
            instrument.OutputVoltageDigitizerCmds.ViewSource();
            instrument.OutputCurrentDigitizerCmds.ViewSource();
            instrument.OutputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage / 3);
            instrument.OutputCurrentDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().MaximumOutputCurrent / 3);
            instrument.InputCurrentDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().LimitInputCurr / 3);
            instrument.InputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);

            #endregion

            #region Rerun measurement of the start up time until a proper scaling is found
            while (scaling * 2 > riseTime)
            {
                instrument.InstrumentCmds.SetTimebaseScale(scaling);
                instrument.InstrumentCmds.SetTimebasePosition(4 * scaling);        //Signal on the screen is shifted to the left by 4 div (Display units)

                triggerAtLoadChange(instrument.InstrumentCmds);

                if ( powerSupply.GetType() == typeof(ACPSChanCmds) )
                {
                    instrument.InputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 2);
                    instrument.InputVoltageDigitizerCmds.SetVerticalOffset(0);
                }
                else
                {
                    instrument.InputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                }
                instrument.OutputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                instrument.InputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                instrument.OutputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();

                //Measure the Rise Time of the current
                instrument.OutputCurrentDigitizerCmds.SetRisetime();
                instrument.OutputVoltageDigitizerCmds.SetThresholds(99, 50, 1);
                TestPlan.Sleep(1000);

                double Marker1 = instrument.InstrumentCmds.GetMarker(CustomSettings.MarkerSelection.X1);
                double Marker2 = instrument.InstrumentCmds.GetMarker(CustomSettings.MarkerSelection.X2);

                riseTime = Marker2 - Marker1;
                scaling = scaling / 2;
            }
            #endregion

            #region Fetch the waveform data from the instrument
            inputVoltageWave = instrument.InputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputVoltageWave = instrument.OutputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            inputCurrentWave = instrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputCurrentWave = instrument.OutputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            #endregion

            #region Process the results
            highLoadVoltage = electronicLoad.GetRmsVoltage();

            //Calculation
            percentageVoltageDrop = -((highLoadVoltage / lowLoadVoltage) - 1) * 100;


            GetParent<Run_PS_Testplan>().Results.PublishTable("Load change\n(Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() + ")", new List<string> { "Time (s)", "Input voltage (V)", "Output voltage (V)", "Input current (I)", "Output current(I)" }, inputVoltageWave.timeAxisValues.ToArray(), inputVoltageWave.yAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());

            #endregion
        }

        private void triggerAtLoadChange(IWaveformDisplayInstrCmds waveformInstrCmds)
        {
            waveformInstrCmds.SetSingle();

            //Apply low load
            electronicLoad.ApplyCCLoad(lowLoadcurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
            TestPlan.Sleep(5000);
            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(highLoadcurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
            TestPlan.Sleep(2000);
        }
    }
}