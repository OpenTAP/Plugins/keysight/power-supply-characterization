﻿#define debug

// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Source Effect", Group: "Switching Mode Power Supplies", Description: "Insert a description here")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class Source_effect : TestStep
    {
        const double startTimebase = 0.01;

        private waveform inputVoltageWave;
        private waveform outputVoltageWave;
        private waveform inputCurrentWave;
        private waveform outputCurrentWave;

        #region MinInputVoltage declaration
        [Display(Name: "Minimum Input Voltage", Order: 1)]
        [Unit("V")]
        public double MinInputVoltage { get; set; }
        #endregion

        #region MaxInputVoltage declaration
        [Display(Name: "Maximum Input Voltage", Order: 2)]
        [Unit("V")]
        public double MaxInputVoltage { get; set; }
        #endregion

        #region LoadPercentage declaration
        [Display(Name: "Load", Description: "Loadcurrent related to the maximum rated output current of the DUT.\n Preset to 50% to run the test as described in the IEC61204.", Order: 3)]
        [Unit("%")]
        public double LoadPercentage { get; set; }
        #endregion

        //Instruments needed
        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;
        public Source_effect()
        {
            LoadPercentage = 50;

            #if debug
            MaxInputVoltage = 16;
            MinInputVoltage = 12;
            #endif
        }

        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;

            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;


            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            powerSupply.SetVoltageRange(this.MaxInputVoltage);

            measureWithPowerConverter();

            if (powerAnalyzer.InstrumentCmds != null)
            {
                measure(powerAnalyzer);
            }
            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }
        }

        public void measureWithPowerConverter()
        {
            double voltage1 = electronicLoad.GetRmsVoltage();

            powerSupply.SetVoltage(MaxInputVoltage);

            TestPlan.Sleep(2000);

            double voltage2 = electronicLoad.GetRmsVoltage();

            double outputchange = (voltage2 / voltage1) - 1;
            double outputchangePercent = outputchange * 100;

            GetParent<Run_PS_Testplan>().Results.Publish("Source effect (Instruments: " + powerSupply.InstrumentCommands.instrument.Name + ", "+ electronicLoad.InstrumentCommands.instrument.Name +", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new { stroutputchangePercent = outputchangePercent });

            UpgradeVerdict(Verdict.Pass);
        }

        public void measure(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            #region Set Instrumentsettings

            if(waveformInstrument.InstrumentCmds.GetType() == typeof(PACmds))
            {
                ((PACmds)(waveformInstrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM3);
                ((PADigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetHWRange(2);
                
                ((PADigitizerCmds)(waveformInstrument.OutputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
                ((PADigitizerCmds)(waveformInstrument.InputVoltageDigitizerCmds)).SetHWRange(this.MaxInputVoltage*Math.Sqrt(2));
            }

            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerSource();
            waveformInstrument.InstrumentCmds.SetSlope(CustomSettings.SlopeSelection.POSitive);
            waveformInstrument.InstrumentCmds.SetTriggerType(CustomSettings.TriggerType.EDGE);

            waveformInstrument.InstrumentCmds.SetTimebaseScale(startTimebase);
            waveformInstrument.InstrumentCmds.SetTimebasePosition(startTimebase * 3);

            waveformInstrument.InputCurrentDigitizerCmds.SetInput(true);
            waveformInstrument.InputVoltageDigitizerCmds.SetInput(true);
            waveformInstrument.OutputCurrentDigitizerCmds.SetInput(true);
            waveformInstrument.OutputVoltageDigitizerCmds.SetInput(true);

            if (powerSupply.GetType() == typeof(ACPSChanCmds))
            {
                waveformInstrument.InputVoltageDigitizerCmds.WaveformCmds.SetWaveformToFullScreen(-this.MaxInputVoltage * Math.Sqrt(2), this.MaxInputVoltage * Math.Sqrt(2));
            }

            #endregion

            //Apply the electronic load
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent * LoadPercentage / 100, GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage * 1.2);

            //trigger on the rising voltage
            triggerAtVoltChange(waveformInstrument);

            #region Scale the Voltage waveform on the screen

            waveformInstrument.OutputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            waveformInstrument.InputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            waveformInstrument.OutputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            waveformInstrument.InputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
            #endregion

            //trigger again on the rising voltage
            triggerAtVoltChange(waveformInstrument);

            #region Fetch the waveform data from the instrument
            inputVoltageWave = waveformInstrument.InputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputVoltageWave = waveformInstrument.OutputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            inputCurrentWave = waveformInstrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputCurrentWave = waveformInstrument.OutputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            #endregion

            #region Process the results
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveforms at input voltage change\n(Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Input voltage (V)", "Output voltage (V)", "Input current (I)", "Output current(I)" }, inputVoltageWave.timeAxisValues.ToArray(), inputVoltageWave.yAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());
            #endregion
        }

        public void triggerAtVoltChange(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            double triggerLevel;

            if (powerSupply.GetType() == typeof(DCPSChanCmds))
            {
                triggerLevel = (MaxInputVoltage - MinInputVoltage) * 0.5 + MinInputVoltage;
            }
            else if(powerSupply.GetType() == typeof(ACPSChanCmds) )
            {
                triggerLevel = ((MaxInputVoltage - MinInputVoltage) * 0.5 + MinInputVoltage)*Math.Sqrt(2);
            }
            else
            {
                throw new NotSupportedException();
            }

            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerLevel(triggerLevel);

            powerSupply.ApplyVoltageSource(MinInputVoltage);

            TestPlan.Sleep(1000);

            waveformInstrument.InstrumentCmds.SetSingle();

            TestPlan.Sleep(1000);

            powerSupply.SetVoltage(MaxInputVoltage);

            TestPlan.Sleep(1000);
        }
    }
}