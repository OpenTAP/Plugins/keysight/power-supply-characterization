﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [DisplayAttribute("r.m.s. Source Current", Group: "Switching Mode Power Supplies", Description: "Measures the r.m.s. source current of the device under test.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]

    public class rms_source_current : TestStep
    {
        #region declaration Delaytime
        [EnabledIf("_IsSCPI_DUT_Used", true, HideIfDisabled = true)]
        [Unit("ms")]
        [DisplayAttribute(Name: "Delay Time", Description: "Time between applying the output current and measuring the input current", Group: "Settings", Order: 2)]
        public int Delaytime{ get; set; }
        #endregion
    
        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;
        PSPluginWaveformInstrumentAdaptor scope;

        public rms_source_current()
        {
            Delaytime = 1000;

            //Rule Validation (example)
           
        }

        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;
            powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;
            scope = GetParent<Run_PS_Testplan>().scopeAdaptor;

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            //Power the DUT
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);

            //Appy electronic load for predefined input current
            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            TestPlan.Sleep(Delaytime);

            ((PADigitizerCmds)(powerAnalyzer.InputCurrentDigitizerCmds)).SetCurrentInput();

            measure(powerSupply.InstrumentCommands.instrument,powerSupply.CurrentMultimeter);

            if (powerAnalyzer.InstrumentCmds != null)
            {
                measure(powerAnalyzer.InstrumentCmds.instrument, powerAnalyzer.InputCurrentDigitizerCmds);
            }
            if (scope.InstrumentCmds != null)
            {
                measure(scope.InstrumentCmds.instrument, scope.InputCurrentDigitizerCmds);
            }

            // ToDo: Add test case code.
            UpgradeVerdict(Verdict.Pass);
        }

        private void measure(CustomInstrument instrument,IMultimeterDigitizerCmds CurrentDigitizer)
        {
            //Measure the source current
            double stepresult = CurrentDigitizer.GetRmsValue();

            GetParent<Run_PS_Testplan>().Results.Publish("r.m.s. Source Current (Instrument: "+ instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() + ")", new { strStepresult = stepresult });

        }
    }
}
