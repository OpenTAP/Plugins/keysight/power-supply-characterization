﻿
// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using System.Diagnostics;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    //This Test Needs a Diod connected to the output of the Power Supply to keep the current from the
    //bulk capacitors to flow back into the Power Supply

    [Display("Hold-up Time", Group: "Switching Mode Power Supplies", Description: "Turns off the input voltage of the power supply and measures how long it will take until the output voltage drops.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class HoldUp_time : TestStep
    {
        private waveform inputVoltageWave;
        private waveform outputVoltageWave;
        private waveform inputCurrentWave;
        private waveform outputCurrentWave;
        private double scaling;
        private double HoldUpTime;
        private string strHoldUpTime;

        //Instruments needed
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;

        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;

        public HoldUp_time()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;
            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;
            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);

            measure(powerAnalyzer);

            if ( scope.InstrumentCmds != null )
            {
                measure(scope);
            }

            UpgradeVerdict(Verdict.Pass);
        }
        
        private void measure(PSPluginWaveformInstrumentAdaptor instrument)
        {
            scaling = 0.1;         //Set scaling to start with
            HoldUpTime = -1;

            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);

            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            #region Power the DUT
            if(powerSupply.GetType() == typeof(DCPSChanCmds))
            {
                powerSupply.SetSenseSource(CustomSettings.SenseSelection.EXTernal);
            }
            else if(powerSupply.GetType() == typeof(ACPSChanCmds))
            {
                powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);
            }
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage, CustomSettings.RelaySelection.NORelay);
           
            #endregion

            #region Adjust Power Analyzer Settings

            if (instrument.InstrumentCmds.GetType() == typeof(PACmds))
            {
                ((PACmds)(instrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM6);
                
                //Set a high range to avoid error messages in cause of exeeding the range.
                ((PADigitizerCmds)(instrument.InputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().InputVoltage);

                if (((PADigitizerCmds)(instrument.InputCurrentDigitizerCmds)).CurrentInput == PADigitizerCmds.CurrentInputType.A2)
                {
                    ((PADigitizerCmds)(instrument.InputCurrentDigitizerCmds)).SetHWRange(2);
                }
                else
                {
                    ((PADigitizerCmds)(instrument.InputCurrentDigitizerCmds)).SetHWRange(50);
                }
                //Set display preferences of the power analyzer
                ((PADigitizerCmds)(instrument.OutputVoltageDigitizerCmds)).SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);

                ((PADigitizerCmds)(instrument.OutputCurrentDigitizerCmds)).SetCurrentInput();
                ((PADigitizerCmds)(instrument.InputCurrentDigitizerCmds)).SetCurrentInput();

                instrument.InputCurrentDigitizerCmds.SetInput(false);
            }

            //Set other preferences for the power 
            instrument.InputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);
            instrument.InputVoltageDigitizerCmds.SetTriggerSource();
            instrument.InstrumentCmds.SetTriggerType(CustomSettings.TriggerType.EDGE);
            instrument.InstrumentCmds.SetSlope(CustomSettings.SlopeSelection.NEGative);
            instrument.InputVoltageDigitizerCmds.SetTriggerLevel(GetParent<Run_PS_Testplan>().InputVoltage * 0.85);

            instrument.OutputVoltageDigitizerCmds.ViewSource();
            instrument.InputVoltageDigitizerCmds.ViewSource();
            instrument.OutputCurrentDigitizerCmds.ViewSource();
            instrument.InputCurrentDigitizerCmds.ViewSource();

            #endregion

            #region Rerun measurement of the start up time until a proper scaling is found
            while (scaling * 2 > HoldUpTime)
            {
                instrument.InstrumentCmds.SetTimebaseScale(scaling);
                instrument.InstrumentCmds.SetTimebasePosition(4 * scaling);        //Signal on the screen is shifted to the left by 4 div (Display units)

                triggerAtTurnOff(instrument.InstrumentCmds);

                instrument.InputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                instrument.OutputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                instrument.InputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                instrument.OutputCurrentDigitizerCmds.WaveformCmds.ZoomToFullWaveform();

                //Measure the Hold Up Time
                instrument.OutputVoltageDigitizerCmds.SetFalltime();
                instrument.OutputVoltageDigitizerCmds.SetThresholds(99, 50, 1);
                TestPlan.Sleep(1000);
                double Marker1 = instrument.InstrumentCmds.GetMarker(CustomSettings.MarkerSelection.X1);


                instrument.InputVoltageDigitizerCmds.SetFalltime();
                instrument.InputVoltageDigitizerCmds.SetThresholds(99, 50, 1);
                TestPlan.Sleep(1000);
                double Marker2 = instrument.InstrumentCmds.GetMarker(CustomSettings.MarkerSelection.X1);

                HoldUpTime = Marker1 - Marker2;
                scaling = scaling / 2;
            }
            #endregion

            #region Fetch the waveform data from the power analyzer
            inputVoltageWave = instrument.InputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputVoltageWave = instrument.OutputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            inputCurrentWave = instrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputCurrentWave = instrument.OutputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            #endregion

            #region Process the results
            GetParent<Run_PS_Testplan>().Results.Publish("Hold Up Time (Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName()+")", new { strHoldUpTime = HoldUpTime });
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveforms at turn off\n(Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Input voltage (V)", "Output voltage (V)", "Input current (A)", "Output current (A)" }, inputVoltageWave.timeAxisValues.ToArray(), inputVoltageWave.yAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());

            /*
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveform of the output voltage at turn off\n(Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Output voltage (V)" }, inputVoltageWave.timeAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray());
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveform of the input current at turn off\n(Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Input current (A)" }, inputCurrentWave.timeAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray());
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveform of the output current at turn off\n(Instrument: " + instrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Output current (A)" }, outputCurrentWave.timeAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());
            */
            #endregion
        }


        public void triggerAtTurnOff(IWaveformDisplayInstrCmds waveformInstrCmds)
        {
            TestPlan.Sleep(1000);
            powerSupply.SetOutput_State(true);
            TestPlan.Sleep(1000);
            waveformInstrCmds.SetSingle();
            TestPlan.Sleep(1000);

            //Set output voltage of the Power Supply
            powerSupply.SetOutput_State(false);
            TestPlan.Sleep(2000);
        }
    }
}