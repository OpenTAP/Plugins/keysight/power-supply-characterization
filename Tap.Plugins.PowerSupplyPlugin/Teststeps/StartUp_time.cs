﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.PSPluginRecources;
using Tap.Plugins.PowerSupplyPlugin.OtherClasses;

namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    [Display("Start-up Time", Group: "Switching Mode Power Supplies", Description: "Turns on the input voltage of the power supply and measures how long it will take until the output voltage rises.")]
    [AllowAsChildIn(typeof(Run_PS_Testplan))]
    public class StartUp_time : TestStep
    {
        private double scaling;
        private double StartUpTime;
        private string strStartUp_time;

        //Instruments needed
        PSPluginWaveformInstrumentAdaptor scope;
        PSPluginWaveformInstrumentAdaptor powerAnalyzer;
        PSChanCmds powerSupply;
        ElLoadChanCmds electronicLoad;

        private waveform inputVoltageWave;
        private waveform outputVoltageWave;
        private waveform inputCurrentWave;
        private waveform outputCurrentWave;

        public StartUp_time()
        {
            
        }

        public override void PrePlanRun()
        {
            //Initialize Instrumentchannels
            powerSupply = GetParent<Run_PS_Testplan>().PSChannelCmds;
            electronicLoad = GetParent<Run_PS_Testplan>().LoadChannelCmds;

            this.powerAnalyzer = GetParent<Run_PS_Testplan>().powerAnalyzerAdaptor;
            this.scope = GetParent<Run_PS_Testplan>().scopeAdaptor;

            base.PrePlanRun();
        }

        public override void Run()
        {
            GetParent<Run_PS_Testplan>().ResetAllInstruments();

            //Set range for AC Power Supplies
            electronicLoad.Init(GetParent<Run_PS_Testplan>().MaximumOutputCurrent);
            powerSupply.SetVoltageRange(GetParent<Run_PS_Testplan>().InputVoltage);

            measure(powerAnalyzer);

            if (scope.InstrumentCmds != null)
            {
                measure(scope);
            }

            UpgradeVerdict(Verdict.Pass);
        }

        public void measure(PSPluginWaveformInstrumentAdaptor waveformInstrument)
        {
            scaling = 1;         //Set scaling to start with
            StartUpTime = -1;

            //Appy electronic load for predefined input current
            electronicLoad.ApplyCCLoad(GetParent<Run_PS_Testplan>().MaximumOutputCurrent, 1.2 * GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);

            #region Adjust instrument Display Settings
            if (waveformInstrument.InstrumentCmds.GetType() == typeof(PACmds))
            {
                ((PACmds)(waveformInstrument.InstrumentCmds)).SetDisplayLayout(CustomSettings.LayoutSelection.FORM6);
                ((PADigitizerCmds)(waveformInstrument.InputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().InputVoltage);
                ((PADigitizerCmds)(waveformInstrument.InputCurrentDigitizerCmds)).SetHWRange(50);      //Set Current Range to its maximum to avoid warnings on the scope
                ((PADigitizerCmds)(waveformInstrument.OutputVoltageDigitizerCmds)).SetHWRange(GetParent<Run_PS_Testplan>().ExpectedDUTOutputVoltage);
                waveformInstrument.InputCurrentDigitizerCmds.SetInput(false);
            }

            waveformInstrument.InputVoltageDigitizerCmds.ViewSource();
            waveformInstrument.InputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);
            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerSource();
            waveformInstrument.InstrumentCmds.SetTriggerType(CustomSettings.TriggerType.EDGE);
            waveformInstrument.InstrumentCmds.SetSlope(CustomSettings.SlopeSelection.POSitive);
            waveformInstrument.InputVoltageDigitizerCmds.SetTriggerLevel(GetParent<Run_PS_Testplan>().InputVoltage * 0.1);

            waveformInstrument.OutputVoltageDigitizerCmds.ViewSource();
            waveformInstrument.OutputVoltageDigitizerCmds.SetVerticalScale(GetParent<Run_PS_Testplan>().InputVoltage / 3);
            #endregion

            #region Power the DUT
            if (powerSupply.GetType() == typeof(DCPSChanCmds))
            {
                powerSupply.SetSenseSource(CustomSettings.SenseSelection.EXTernal);
            }
            powerSupply.SetVoltage(GetParent<Run_PS_Testplan>().InputVoltage);
            #endregion

            #region Rerun measurement of the start up time until a proper scaling is found
            while (scaling * 5 > StartUpTime)
            {
                waveformInstrument.InstrumentCmds.SetTimebaseScale(scaling);
                waveformInstrument.InstrumentCmds.SetTimebasePosition(4 * scaling);        //Signal on the screen is shifted to the left by 4 div (Display units)

                triggerAtPowerOn(waveformInstrument.InstrumentCmds);

                TestPlan.Sleep(10000);     //Wait until there is a full waveform on the screen

                waveformInstrument.InputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();
                waveformInstrument.OutputVoltageDigitizerCmds.WaveformCmds.ZoomToFullWaveform();

                //Measure the StartUp_time
                waveformInstrument.OutputVoltageDigitizerCmds.SetRisetime();
                waveformInstrument.OutputVoltageDigitizerCmds.SetThresholds(99, 50, 1);

                TestPlan.Sleep(2000);
                StartUpTime = waveformInstrument.InstrumentCmds.GetMarker(CustomSettings.MarkerSelection.X1);

                scaling = scaling / 2;
            }
            #endregion

            #region Fetch the waveform data from the power analyzer
            inputVoltageWave = waveformInstrument.InputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputVoltageWave = waveformInstrument.OutputVoltageDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            inputCurrentWave = waveformInstrument.InputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            outputCurrentWave = waveformInstrument.OutputCurrentDigitizerCmds.GetWaveformData(GetParent<Run_PS_Testplan>().Points);
            #endregion

            #region Process the results
            GetParent<Run_PS_Testplan>().Results.Publish("Start up time (Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() + ")", new { strStartUp_time = StartUpTime });
            GetParent<Run_PS_Testplan>().Results.PublishTable("Waveforms during start up\n(Instrument: " + waveformInstrument.InstrumentCmds.instrument.Name + ", Executed in: " + this.GetParent<Run_PS_Testplan>().GetFormattedName() + "->" + this.GetFormattedName() +")", new List<string> { "Time (s)", "Input voltage (V)", "Output voltage (V)", "Input current (I)", "Output current (I)" }, inputVoltageWave.timeAxisValues.ToArray(), inputVoltageWave.yAxisValues.ToArray(), outputVoltageWave.yAxisValues.ToArray(), inputCurrentWave.yAxisValues.ToArray(), outputCurrentWave.yAxisValues.ToArray());
            #endregion
        }

        private void triggerAtPowerOn(IWaveformDisplayInstrCmds waveformInstrCmds)
        {
            powerSupply.SetVoltage(0);
            TestPlan.Sleep(1000);           //Wait a second
            waveformInstrCmds.SetSingle();
            TestPlan.Sleep(2000);           //Wait a second
            powerSupply.ApplyVoltageSource(GetParent<Run_PS_Testplan>().InputVoltage);
            TestPlan.Sleep(2000);           //Wait a second
        }
    }
}
