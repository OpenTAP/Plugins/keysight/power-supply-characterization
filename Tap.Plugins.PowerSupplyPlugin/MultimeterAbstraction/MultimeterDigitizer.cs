﻿using Agilent.CommandExpert.ScpiNet.AgAC6800_A_01_00_0067;
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Digitizer;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.Multimeter
{
    public class MultimeterDigitizerCmds : DigitizerCmds, IMultimeterDigitizerCmds
    {
        public enum Unity { CURR, VOLT }
        private Unity unitySelection { get; set; }

        public MultimeterDigitizerCmds(ChanCmds _channelCmds, Unity input) : base(_channelCmds)
        {
            unitySelection = input;
        }

        public double GetRmsValue()
        {
            double[] returnvalue = { -9999 };

            if ( this.channelCmds.InstrumentCommands.device.GetType() == typeof(AgN6705) )
            {
                if (unitySelection == Unity.CURR)
                {
                    ((AgN6705)(this.channelCmds.InstrumentCommands.device)).SCPI.MEASure.SCALar.CURRent.DC.Query("@" + this.channelCmds.channel, out returnvalue);
                }
                else
                {
                    ((AgN6705)(this.channelCmds.InstrumentCommands.device)).SCPI.MEASure.SCALar.VOLTage.DC.Query("@" + this.channelCmds.channel, out returnvalue);
                }
            }
            else if (this.channelCmds.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                if (unitySelection == Unity.CURR)
                {
                    ((AgAC6800)(this.channelCmds.InstrumentCommands.device)).SCPI.MEASure.CURRent.DC.Query(out returnvalue[0]);
                }
                else
                {
                    ((AgAC6800)(this.channelCmds.InstrumentCommands.device)).SCPI.MEASure.VOLTage.DC.Query(out returnvalue[0]);
                }
            }

            return returnvalue[0];
        }
    }
}
