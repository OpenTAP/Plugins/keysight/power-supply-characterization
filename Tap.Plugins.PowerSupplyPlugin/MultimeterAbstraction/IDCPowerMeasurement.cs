﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tap.Plugins.PowerSupplyPlugin.Multimeter
{
    public interface IDCPowerMeasurement
    {
        double PerformDCPowerMeasurementRoutine();
    }
}
