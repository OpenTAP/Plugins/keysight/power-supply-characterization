﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    /// <summary>
    /// The Class ElLoad represents a channel of an instrument with more than one output
    /// </summary>
    public class ElLoadChanCmds : PCChanCmds, IDCPowerMeasurement
    {
        new private ELoadCmds InstrumentCommands;
        new const ModuleType type = ModuleType.DC;
        private DCPCChanCmds DCCmds;

        //Constructor
        public ElLoadChanCmds(ELoadCmds _InstrumentCommands, int _channel) : base(_InstrumentCommands, _channel)
        {
            DCCmds = new DCPCChanCmds(_InstrumentCommands, _channel);

            this.InstrumentCommands = _InstrumentCommands;
        }

        //Source Subsystem
        public override void SetAutoRange(CustomSettings.VoltCurrSelection unit, bool enabled)
        {
            this.SetAutoRange(unit, enabled);
        }
        
        public override void SetCurrentRange(double current)
        {
            if(this.GetSourceCurrentLevel() > GetCurrentRange())
            {
                SetSourceCurrentLevel(GetCurrentRange());
            }

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (current > 0.001)
                {
                    ((AgN6705)this.InstrumentCommands.device).SCPI.SOURce.CURRent.RANGe.Command(current, "@" + this.channel);
                }
                else
                {
                    ((AgN6705)this.InstrumentCommands.device).SCPI.SOURce.CURRent.RANGe.Command(0.001, "@" + this.channel);
                }
            }
        }

        public double GetCurrentRange()
        {
            double[] rangeArray = { -9999 };

            ((AgN6705)this.InstrumentCommands.device).SCPI.SOURce.CURRent.RANGe.Query("@" + this.channel, out rangeArray);

            return rangeArray[0];
        }

        public override void SetListResponse(CustomSettings.ListResponseSelection type)
        {
            this.InstrumentCommands.instrument.SetListResponse(this.channel, type);
        }



        /// <summary>
        /// Sets the voltage limit
        /// </summary>
        /// <param name="voltage"></param>
        public override void SetVoltageLimit(double voltage)
        {
            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (this.GetChanModel().Contains("N678"))
                {
                    string[] emulationModeArray = { "" };
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.EMULation.Query("@" + this.channel, out emulationModeArray);

                    if (emulationModeArray[0] == "CCL")     //CCL stands for Constant Current Load
                    {
                        ((AgN6705)InstrumentCommands.device).SCPI.SOURce.VOLTage.LIMit.POSitive.IMMediate.AMPLitude.Command(voltage, "@" + this.channel);
                    }
                    else
                    {
                        throw new Exception();          //Emulation mode must support setting the voltage level
                    }
                }
                else
                {
                    throw new Exception();      //Module does not support limiting the voltage
                }
            }
        }


        //Measure Subsystem

        public double PerformDCPowerMeasurementRoutine()
        {
            double returnvalue = -9999;

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                double[] outputArray = { -9999 };

                if (this.GetChanModel().Contains("N678"))
                {
                    ((AgN6705)(this.InstrumentCommands.device)).SCPI.MEASure.SCALar.POWer.DC.Query("@" + this.channel, out outputArray);
                    returnvalue = -outputArray[0];
                }
                else if(this.GetChanModel().Contains("N679"))
                {
                    ((AgN6705)(this.InstrumentCommands.device)).SCPI.MEASure.SCALar.POWer.DC.Query("@" + this.channel, out outputArray);
                    returnvalue = outputArray[0];
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            else
            {
                throw new NotImplementedException();
            }

            return returnvalue;
        }

        public override double GetRmsCurrent() { return this.DCCmds.GetRmsCurrent(); }
        public override void SetEmulationMode(CustomSettings.EMType type) { this.DCCmds.SetEmulationMode(type); }
        public override void SetVoltage(double voltage) { this.DCCmds.SetVoltage(voltage); }
        public override void SetVoltageLevel(double voltage) { this.DCCmds.SetVoltageLevel(voltage); }

        public void Init(double currentRange)
        {
            if (GetChanModel().StartsWith("N678"))      //Only N678XA-Modules support differen emulation modes
            {
                this.SetEmulationMode(CustomSettings.EMType.CCLoad);
            }
            else if(GetChanModel().StartsWith("N679"))
            {
                //do nothing
            }
            else
            {
                throw new NotSupportedException();
            }

            this.SetCurrentRange(currentRange);
        }

        //Methods which combine several SCPIs
        public enum SetRange { Set, DoNotSet}
        /// <summary>
        /// Applys a constant current load.
        /// </summary>
        /// <param name="current">
        /// Input loadcurrent of the electronic load.
        /// </param>
        /// <param name="voltageLimit">
        /// Maximum Voltage at the electronic load.
        /// </param>
        public void ApplyCCLoad(double current, double voltageLimit)
        {
            if (GetChanModel().StartsWith("N678"))
            {
                this.SetVoltageLimit(voltageLimit);         //Limit the Voltage to the expected outputVoltage plus 20%
            }
            this.SetSourceCurrentLevel(-current);       //Apply negative source current (which is a load current)
            this.SetOutput_State(true);
        }
    }
}
