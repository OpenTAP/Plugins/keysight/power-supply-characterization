﻿using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands
{
    public class DCPCChanCmds : ChanCmds
    {
        public DCPCChanCmds(PCCmds _InstrumentCommands, int _channel) : base(_InstrumentCommands, _channel)
        {
           
        }

        //Output Subsystem
        public double GetRmsCurrent()      //Exactly the same Function as for the DC Power Supply
        {
            double[] returnvalueArray = { -9999 };

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.MEASure.SCALar.CURRent.ACDC.Query("@" + this.channel, out returnvalueArray);
            }
            else
            {
                throw new NotSupportedException();
            }

            return returnvalueArray[0];
        }

        public void SetEmulationMode(CustomSettings.EMType type)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.EMULation.Command(type.ToString("g"), "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }


        public void SetVoltage(double voltage)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.VOLTage.LEVel.IMMediate.AMPLitude.Command(voltage, "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public void SetVoltageLevel(double voltage)
        {
            double[] voltageArray = { voltage };

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.LIST.VOLTage.LEVel.Command(voltageArray, "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
}
