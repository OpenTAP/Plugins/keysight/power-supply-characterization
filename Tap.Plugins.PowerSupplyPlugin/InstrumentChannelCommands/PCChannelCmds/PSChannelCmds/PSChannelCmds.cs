﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public abstract class PSChanCmds: PCChanCmds
    {
        new public PSCmds InstrumentCommands;

        //Constructor
        public PSChanCmds(PSCmds _InstrumentCommands, int _channel) : base((PCCmds)_InstrumentCommands, _channel)
        {
            InstrumentCommands = _InstrumentCommands;
        }

        //Abort Subystem

        /// <summary>
        /// Cancels any triggered actions. It also resets the WTG-tran bit in the Operation Condition Status register.
        /// Uses the "ABORt:TRANsient (@chanlist)" command.
        /// </summary>
        public void CancelTriggeredMeasurements()
        {
            this.InstrumentCommands.instrument.ScpiCommand(":ABORt:TRANsient (@" + this.channel.ToString() + ")");
        }


        //Initiate subsystem

        /// <summary>
        /// Initiates the transient trigger system. When initiated, an event on the selected trigger source causes the specified triggered action to occur on the output.
        /// Uses the "INITiate[:IMMediate]:TRANsient (@chanlist)" command.
        /// </summary>
        public void InitiateMeasurementTrigger()
        {
            this.InstrumentCommands.instrument.ScpiCommand(":INITiate:IMMediate:TRANsient (@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Applys a voltage and activates the output.
        /// </summary>
        /// <param name="voltage"></param>
        /// <param name="relay"></param>
        public abstract void ApplyVoltageSource(double voltage, CustomSettings.RelaySelection relay = CustomSettings.RelaySelection.YESRelay);
    }
}
