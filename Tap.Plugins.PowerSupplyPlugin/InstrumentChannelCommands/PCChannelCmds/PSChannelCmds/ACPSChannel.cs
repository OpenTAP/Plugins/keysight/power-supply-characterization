﻿using Agilent.CommandExpert.ScpiNet.AgAC6800_A_01_00_0067;
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public class ACPSChanCmds : PSChanCmds, IACPowerMeasurement
    {
        new ACPSCmds InstrumentCommands;
        new const ModuleType type = ModuleType.AC;

        //Constructor
        public ACPSChanCmds(ACPSCmds _InstrumentCommands, int _channel) : base((PSCmds)_InstrumentCommands, _channel)
        {
            this.InstrumentCommands = _InstrumentCommands;
        }

        //Source Subsystem

        public override void SetAutoRange(CustomSettings.VoltCurrSelection unit, bool isEnabled)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                if (unit == CustomSettings.VoltCurrSelection.CURRent)
                {
                    throw new NotSupportedException();
                }
                else
                {
                    ((AgAC6800)InstrumentCommands.device).SCPI.SOURce.VOLTage.RANGe.AUTO.Command(isEnabled);
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        public override void SetCurrentRange(double current)
        {
            throw new NotImplementedException();
        }
        public override void SetListResponse(CustomSettings.ListResponseSelection type)
        {
            throw new NotImplementedException();
        }
        public override void SetVoltageLevel(double voltage)
        {
            throw new NotImplementedException();
        }
        public override void SetVoltageLimit(double voltage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Command is not Supported by the AC Power Supply AC6801B
        /// </summary>
        /// <param name="voltage"></param>
        public override void SetVoltageRange(double voltage)
        {
            ((AgAC6800)(this.InstrumentCommands.device)).SCPI.SOURce.VOLTage.RANGe.UPPer.Command(voltage);
        }
        public override void SetVoltage(double voltage)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                ((AgAC6800)InstrumentCommands.device).SCPI.SOURce.VOLTage.LEVel.IMMediate.AMPLitude.Command(voltage);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        public override void SetEmulationMode(CustomSettings.EMType type)
        {
            throw new NotSupportedException();
        }


        /// <summary>
        /// Applys a voltage and activates the output.
        /// </summary>
        /// <param name="voltage"></param>
        /// <param name="relay"></param>
        public override void ApplyVoltageSource(double voltage, CustomSettings.RelaySelection relay = CustomSettings.RelaySelection.YESRelay)
        {
            this.SetVoltage(voltage);

            if (GetOutputState() == false)
            {
                this.SetOutput_State(true);
            }
        }

        //Output Subsystem

        public override void SetOutput_State(bool state, CustomSettings.RelaySelection relay = CustomSettings.RelaySelection.YESRelay)
        {
            if(relay == CustomSettings.RelaySelection.NORelay)
            {
                throw new NotSupportedException();      //The AC Power Supply AC6801B does not support the NoRelay option
            }

            if (this.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                ((AgAC6800)InstrumentCommands.device).SCPI.OUTPut.STATe.Command(state);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        public override void SetPowerLimit(double current)
        {
            throw new NotSupportedException();
        }
        public override double GetRmsCurrent()
        {
            double returnvalue = -9999;

            if (this.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                ((AgAC6800)InstrumentCommands.device).SCPI.MEASure.CURRent.ACDC.Query(out returnvalue);
            }
            else
            {
                throw new NotSupportedException();
            }

            return returnvalue;
        }

        public double PerformACPowerMeasurementRoutine()
        {
            double returnvalue = -9999;

            if (this.InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                ((AgAC6800)(this.InstrumentCommands.device)).SCPI.MEASure.POWer.AC.REAL.Query(out returnvalue);
            }
            else
            {
                throw new NotImplementedException();
            }

            return returnvalue;
        }
    }
}
