﻿using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public class DCPSChanCmds: PSChanCmds, IDCPowerMeasurement
    {
        new DCPSCmds InstrumentCommands;
        new const ModuleType type = ModuleType.DC;
        private DCPCChanCmds DCCmds;

        //Constructor

        public DCPSChanCmds(DCPSCmds _InstrumentCommands, int _channel) : base(_InstrumentCommands, _channel)
        {
            DCCmds = new DCPCChanCmds(_InstrumentCommands, _channel);
            this.InstrumentCommands = _InstrumentCommands;
        }

        //Source Subsystem

        public override void SetAutoRange(CustomSettings.VoltCurrSelection unit, bool enabled)
        {
            throw new NotSupportedException();      //The DC Power Supply N67xx does not support Autoranging
        }

        public override void SetCurrentRange(double current)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.RANGe.Command(current, "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }




        public override void SetListResponse(CustomSettings.ListResponseSelection type)
        {
            this.InstrumentCommands.instrument.SetListResponse(this.channel, type);
        }





        public override void SetVoltageLimit(double voltage)
        {
            if(this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)this.InstrumentCommands.device).SCPI.SOURce.VOLTage.PROTection.LEVel.Command(voltage, "@" + this.channel);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the number of times that the arbitrary waveform is repeated.
        /// Uses the "[SOURce:]ARB:COUNt value, (@chanlist)" command.
        /// </summary>
        public void SetContinousRepeat()
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:COUNt INFinity,(@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Sets the function of the arbitrary waveform generator.
        /// Uses the "[SOURce:]ARB:FUNCtion:SHAPe function, (@chanlist)" command.
        /// </summary>
        /// <param name="shape">
        /// Specifies the function to set. User can choose between the following options:
        /// Step:  Specifies a step 
        /// Ramp: Specifies a ramp 
        /// Staircase:  Specifies a staircase 
        /// Sinusoid: Specifies a sine wave 
        /// PulSe Specifies: a pulse 
        /// Trapezoid:  Specifies a trapezoid 
        /// Exponential: Specifies a exponential waveform 
        /// Udefined:  Specifies a user-defined waveform 
        /// CDwell:  Specifies a constant-dwell waveform 
        /// Sequence: Specifies a sequence of arbitrary waveforms 
        /// None: Specifies no arbitrary waveform 
        /// </param>
        public void SetArbFunctionShape(CustomSettings.ArbFunctionType shape)
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:FUNCtion:SHAPe " + shape.ToString("g") + ",(@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Selects either a voltage or current ARB. Only one type of ARB may be output at a time.
        /// Uses the "[SOURce:]ARB:FUNCtion:TYPE type, (@chanlist)" command.
        /// </summary>
        /// <param name="type">
        /// Used to choose between Voltage or Current.
        /// </param>
        public void SetArbFunctiontype(CustomSettings.VoltCurrSelection type)
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:FUNCtion:TYPE " + type.ToString("g") + ",(@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Specifies the amplitude or peak value of the voltage sine wave. Referenced to V0 in the Sinusoid diagrams.
        /// Uses the "[SOURce:]ARB:VOLTage:SINusoid:AMPLitude value, (@chanlist)" command.
        /// </summary>
        /// <param name="voltage">
        /// Voltage amplitude in Volt to set.
        /// </param>
        public void SetArbVoltageAmplitude(double voltage)
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:VOLTage:SINusoid:AMPLitude " + voltage.ToString() + ",(@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Specifies the frequency of the voltage sine wave in Hertz.
        /// Uses the "[SOURce:]ARB:VOLTage:SINusoid:FREQuency value, (@chanlist)" command.
        /// </summary>
        /// <param name="frequency">
        /// Frequenc to set in Herz.
        /// </param>
        public void SetArbFrequency(double frequency)
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:VOLTage:SINusoid:FREQuency " + frequency.ToString() + ",(@" + this.channel.ToString() + ")");
        }

        /// <summary>
        /// Specifies the offset of the voltage sine wave from zero. Referenced to V1 in the Sinusoid diagrams.
        /// Uses the "[SOURce:]ARB:VOLTage:SINusoid:OFFSet value, (@chanlist)" command.
        /// </summary>
        /// <param name="voltage">
        /// Voltage in Volts to set.
        /// </param>
        public void SetArbVoltageOffset(double voltage)
        {
            this.InstrumentCommands.instrument.ScpiCommand(":SOURce:ARB:VOLTage:SINusoid:OFFSet " + voltage.ToString() + ",(@" + this.channel.ToString() + ")");
        }

        //
        public override double GetRmsCurrent() { return this.DCCmds.GetRmsCurrent(); }
        public override void SetEmulationMode(CustomSettings.EMType type) { this.DCCmds.SetEmulationMode(type); }
        public override void SetVoltage(double voltage) { this.DCCmds.SetVoltage(voltage); }
        public override void SetVoltageLevel(double voltage) { this.DCCmds.SetVoltageLevel(voltage); }

        /// <summary>
        /// Applys a voltage and activates the output.
        /// </summary>
        /// <param name="voltage"></param>
        /// <param name="relay"></param>
        public override void ApplyVoltageSource(double voltage, CustomSettings.RelaySelection relay = CustomSettings.RelaySelection.YESRelay)
        {
            if (voltage != 0)
            {
                {
                    this.SetVoltageRange(voltage);         //Set voltage range manually if autoranging is not available (Needed for the N67xx Power Supplys)
                }
            }
            this.SetVoltage(voltage);

            if (GetOutputState() == false)
            {
                this.SetOutput_State(true);
            }
        }

        //Measure Subsystem
        public double PerformDCPowerMeasurementRoutine()
        {
            double returnvalue = -9999;

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                double[] outputArray = { -9999 };

                ((AgN6705)(this.InstrumentCommands.device)).SCPI.MEASure.SCALar.POWer.DC.Query("@" + this.channel, out outputArray);

                returnvalue = outputArray[0];
            }
            else
            {
                throw new NotImplementedException();
            }

            return returnvalue;
        }
    }
}
