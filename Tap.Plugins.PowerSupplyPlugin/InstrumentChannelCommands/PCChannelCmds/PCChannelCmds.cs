﻿using Agilent.CommandExpert.ScpiNet.AgAC6800_A_01_00_0067;
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    /// <summary>
    /// Represents an instrument channel which is capable of converting power into another type of energy
    /// This can be a power supply (AC or DC), an electronic load or another instrument which converts power
    /// </summary>
    public abstract class PCChanCmds : ChanCmds
    {
        public enum ModuleType { AC, DC }
        public ModuleType type;

        public MultimeterDigitizerCmds VoltageMultimeter;
        public MultimeterDigitizerCmds CurrentMultimeter;

        new public PCCmds InstrumentCommands;

        //Constructor

        public PCChanCmds(PCCmds _InstrumentCommands, int _channel) : base((InstrCmds)_InstrumentCommands, _channel)
        {
            VoltageMultimeter = new MultimeterDigitizerCmds(this, MultimeterDigitizerCmds.Unity.VOLT);
            CurrentMultimeter = new MultimeterDigitizerCmds(this, MultimeterDigitizerCmds.Unity.CURR);


            if (_InstrumentCommands != null)
            {
                this.InstrumentCommands = _InstrumentCommands;
            }
        }

        //Source subsystem

        /// <summary>
        /// Sets the state of the output remote sense relays.
        /// Uses the "SOURce:VOLTage:SENSe:SOURce" command.
        /// </summary>
        /// <param name="type">
        /// Used to choose between local (Internal) and remote (External) sense.
        /// </param>
        public void SetSenseSource(CustomSettings.SenseSelection type)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.VOLTage.SENSe.SOURce.Command(type.ToString("g"), "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// See the description of the function inside this Method for summary
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="enabled"></param>
        public abstract void SetAutoRange(CustomSettings.VoltCurrSelection unit, bool enabled);

        /// <summary>
        /// Sets the immediate current level and the triggered current level when the output is operation in constant current mode.
        /// Uses the "[SOURce:]CURRent[:LEVel][:IMMediate][:AMPLitude] value, [@chanlist]" command.
        /// 
        /// Needs to be overridden by the AC Power Analyzer class
        /// </summary>
        /// <param name="current">
        /// Current Level in Ampere.
        /// </param>
        public void SetCurrentLimit(double current)
        {
            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (this.GetChanModel().Contains("N678"))      //If this is a channel of the N678 Series...
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LIMit.POSitive.IMMediate.AMPLitude.Command(current, "@" + this.channel.ToString());
                }
                else if(this.GetChanModel().Contains("N676"))
                {
                    this.SetSourceCurrentLevel(current);
                }
            }
            else if(InstrumentCommands.device.GetType() == typeof(AgAC6800))
            {
                ((AgAC6800)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Command(current);
            }
            else
            {
                throw new Exception();
            }
        }

        public abstract void SetVoltageLimit(double voltage);

        public void SetSourceCurrentLevel(double current)
        {
            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (this.GetChanModel().Contains("N678"))
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Command(current, "@" + this.channel.ToString());
                }
                else if(this.GetChanModel().Contains("N679"))
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Command(-current, "@" + this.channel.ToString());    //The N679X is an electronic Load. Therefore the current needs to be negative.
                }
                else if(this.GetChanModel().Contains("N676"))   //Does only support positive currents
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Command(current, "@" + this.channel.ToString());
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public double GetSourceCurrentLevel()
        {
            double[] currentArray = { -9999 };
            double current = -9999;

            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (this.GetChanModel().Contains("N678"))
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Query(null, "@" + this.channel.ToString(),out currentArray);
                    current = currentArray[0];
                }
                else if (this.GetChanModel().Contains("N679"))
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Query(null, "@" + this.channel.ToString(), out currentArray);    //The N679X is an electronic Load. Therefore the current needs to be negative.
                    current = -currentArray[0];
                }
                else if (this.GetChanModel().Contains("N676"))   //Does only support positive currents
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.SOURce.CURRent.LEVel.IMMediate.AMPLitude.Query(null, "@" + this.channel.ToString(), out currentArray);
                    current = currentArray[0];
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            else
            {
                throw new NotSupportedException();
            }
            return current;
        }

        /// <summary>
        /// Sets the output current range on models that have multiple ranges.
        /// Uses the "[SOURce:]CURRent:RANGe value, [@chanlist]" command.
        /// </summary>
        /// <param name="current">
        /// Current Limit in Ampere to set
        /// </param>
        public abstract void SetCurrentRange(double current);

        /// <summary>
        /// Specifies how the list responds to triggers.
        /// Uses the "[SOURce:]LIST:STEP ONCE|AUTO, [@chanlist]" command.
        /// </summary>
        /// <param name="type">
        /// User can choose between the following options:
        /// 
        /// Once - the output remains at the present step until a trigger advances it to the next step.
        /// Triggers that arrive during the dwell time are ignored.
        /// 
        ///Auto - the output automatically advances to each step, after the receipt of an initial starting trigger.
        ///Steps are paced by the dwell list. As each dwell time elapses, the next step is immediately output.
        /// </param>
        public abstract void SetListResponse(CustomSettings.ListResponseSelection type);

        /// <summary>
        /// Specifies the dwell time for each list step. Dwell time is the time that the output will remain at a specific step.
        /// Uses the "[SOURce:]LIST:DWELl value{,value}, (@chanlist)" command.
        /// </summary>
        /// <param name="time">
        /// Determines the dwell time.
        /// </param>
        public void SetDwellTime(double time)
        {
            double[] timeArray = { time };

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.LIST.DWELl.Command(timeArray, "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Warning: Command not usable yet.
        /// 
        /// Specifies the setting for each list step.  Values are specified in either amperes or volts.
        /// A comma-delimited list of up to 512 steps may be programmed.
        /// Uses the "[SOURce:]LIST:VOLTage[:LEVel] value,{value}, [@chanlist]" command.
        /// </summary>
        /// <param name="voltage">
        /// 
        /// </param>
        public abstract void SetVoltageLevel(double voltage);




        /// <summary>
        /// Sets the transient mode. This determines what happens to the output voltage when the transient system is initiated and triggered.
        /// Uses the "[SOURce:]VOLTage:MODE FIXed|STEP|LIST|ARB, (@chanlist)" command.
        /// </summary>
        /// <param name="type">
        /// User can choose between the following options:
        /// 
        /// Fixed - keeps the output voltage at its immediate value. 
        /// Step - steps the output to the triggered level when a trigger occurs.
        /// List - causes the output to follow the list values when a trigger occurs.
        /// Arb - causes the output to follow the arbitrary waveform values when a trigger occurs.
        /// </param>
        public void SetTransientMode(CustomSettings.TransientModeSelection type)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.VOLTage.MODE.Command(type.ToString("g"), "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Sets the list repeat count. This sets the number of times that a list is executed before it completes. 
        /// Uses the "[SOURce:]LIST:COUNt value, (@chanlist)" command.
        /// </summary>
        /// <param name="type">
        /// User can select between:
        /// 
        /// Min: Minimum number of repeats
        /// Max: Maximum number of repeats
        /// Infinity: Endless repeat of the List.
        /// </param>
        public void SetListRepeatCount(CustomSettings.ListRepeatCountSelection type)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.LIST.COUNt.Command(type.ToString("g"), "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Sets the immediate voltage level and the triggered voltage level when the output is in constant voltage mode.
        /// Can be overridden by classes which inherit from PowerSupply
        /// Uses the "[SOURce:]VOLTage[:LEVel][:IMMediate][:AMPLitude] value, [@chanlist]" command.
        /// </summary>
        /// <param name="voltage">
        /// Voltage to set in Volts
        /// </param>
        public abstract void SetVoltage(double voltage);

        /// <summary>
        /// Sets the output voltage range on models that have multiple ranges.
        /// Uses the "[SOURce:]VOLTage:RANGe value, [@chanlist]" command.
        /// </summary>
        /// <param name="voltage">
        /// Voltage limit to set in Volts
        /// </param
        public virtual void SetVoltageRange(double voltage)
        {
            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.VOLTage.RANGe.Command(voltage, "@" + this.channel.ToString());
            }
        }

        /// <summary>
        /// Specifies the emulation type for models N678xA.
        /// Warning: Does not work for other Power Converter Units.
        /// </summary>
        /// <param name="">
        /// Desired emulation type.
        /// </param>
        public abstract void SetEmulationMode(CustomSettings.EMType type);


        //Output Subsystem

        /// <summary>
        /// Enables or disables the output.
        /// Uses the "OUTPut[:STATe] 0|OFF|1|ON [,NORelay], [@chanlist]" command.
        /// </summary>
        /// <param name="state">
        /// Used to choose the state of the relay (true: on, false: off).
        /// </param>
        /// <param name="relay">
        /// Optional: Relays will not disconnect the Power Amplifier of the Instrument from the output if set to NoRelay.
        /// </param>
        public virtual void SetOutput_State(bool state, CustomSettings.RelaySelection relay = CustomSettings.RelaySelection.YESRelay)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (relay == CustomSettings.RelaySelection.NORelay)
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.OUTPut.STATe.Command(state, "NORelay", "@" + this.channel);
                }
                else
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.OUTPut.STATe.Command(state, null, "@" + this.channel);
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public bool GetOutputState()
        {
            bool state = false;

            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.OUTPut.STATe.Query("(@" + this.channel + ")", out state);
            }

            return state;
        }

        /// <summary>
        /// Sets the power limit on output channels.
        /// Uses the "[SOURce:]POWer:LIMit value, [@chanlist]" command.
        /// </summary>
        /// <param name="current">
        /// Current limit in Ampere
        /// </param>
        public virtual void SetPowerLimit(double power)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SOURce.POWer.LIMit.Command(power, "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public void SetTmode(CustomSettings.TmodeOption tmodeOption)
        {
            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                if (GetChanModel().StartsWith("N678"))      //Only N678XA-Modules support differen emulation modes
                {
                    ((AgN6705)InstrumentCommands.device).SCPI.OUTPut.STATe.Command(tmodeOption.ToString("g"), null, "(@" + this.channel + ")");
                }
            }
            else if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.OUTPut.STATe.TMODe.Command(tmodeOption.ToString("g"), "(@" + this.channel + ")");
            }
        }


        //Measure Subsystem

        /// <summary>
        /// Initiates, triggers, and returns the total RMS measurement (AC + DC).
        /// Uses the "MEASure[:SCALar]:CURRent:ACDC? (@chanlist)" command.
        /// </summary>
        /// <returns>
        /// The Measured Voltage in Volts.
        /// </returns>
        public double GetRmsVoltage()
        {
            double[] returnArray = { -9999 };

            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.MEASure.SCALar.VOLTage.ACDC.Query("@" + this.channel, out returnArray);
            }
            else
            {
                throw new NotSupportedException();
            }

            return returnArray[0];
        }

        /// <summary>
        /// Initiates, triggers, and returns the averaged current.
        /// Uses the "MEASure[:SCALar]:CURRent:ACDC? [@chanlist]" command.
        /// </summary>
        /// <returns>
        /// The measured current in Ampere.
        /// </returns>
        public abstract double GetRmsCurrent();

        //Trigger Subsystem

        /// <summary>
        /// selects the trigger source for the transient system.
        /// Uses the "TRIGger:TRANsient:SOURce source, (@chanlist)" command.
        /// </summary>
        /// <param name="type">
        /// Used to select a source from the predefined list.
        /// </param>
        public void SetTriggerSource(CustomSettings.TriggerSourceSelection type)
        {
            if (this.InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.TRIGger.TRANsient.SOURce.Command(type.ToString("g"), "@" + this.channel);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        //System Subsystem
        public void GetError(out int errNumber, out string errString)
        {
            errNumber = -1;
            errString = "-1";

            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SYSTem.ERRor.Query(out errNumber, out errString);
            }
        }

        public string GetChanModel()
        {
            string[] modelArray = { "" };

            if (InstrumentCommands.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)InstrumentCommands.device).SCPI.SYSTem.CHANnel.MODel.Query("(@" + this.channel.ToString() + ")", out modelArray);
            }

            return modelArray[0];
        }


        //Combined SCPIs

        /// <summary>
        /// Applys a source voltage with an AC Part which has a frequency of 50 Hz
        /// </summary>
        public void ApplyNoisyVoltage(double offset, double amplitude)
        {
            //Does not work yet
            this.SetVoltage(offset);
            this.SetTransientMode(CustomSettings.TransientModeSelection.LIST);
            this.SetListResponse(CustomSettings.ListResponseSelection.AUTO);
            this.SetTriggerSource(CustomSettings.TriggerSourceSelection.IMMediate);
            this.SetListRepeatCount(CustomSettings.ListRepeatCountSelection.INFinity);
            this.SetDwellTime(262);
            this.SetVoltageLevel(24);
            this.SetOutput_State(true);
            //this.InitiateMeasurementTrigger();
            //this.CancelTriggeredMeasurements();


            /*
            SetArbFunctionShape(CustomSettings.ArbFunctionType.types.Sinusoid);
            SetArbFunctiontype(CustomSettings.VoltCurrSelection.types.Voltage);
            SetArbVoltageOffset(offset);
            SetArbVoltageAmplitude(amplitude);
            SetArbFrequency(50);
            SetContinousRepeat();

            setOutput_State(true);
            */
        }


    }
}
