﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Digitizer;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentChannels
{
    public class PAChanCmds : ChanCmds, IACPowerMeasurement, IDCPowerMeasurement
    {
        public PADigitizerCmds CURRentDigitizer;
        public PAPOWerDigitizerCmds POWerDigitizer;
        public PADigitizerCmds VOLTageDigitizer;

        new public PACmds InstrumentCommands { get; set; }

        public PAChanCmds(PACmds _instrumentCmds,int _channel,PADigitizerCmds.CurrentInputType input):base(_instrumentCmds,_channel)
        {
            this.InstrumentCommands = _instrumentCmds;

            CURRentDigitizer = new PADigitizerCmds(this, CustomSettings.InputSelection.CURRent);
            POWerDigitizer = new PAPOWerDigitizerCmds(this);
            VOLTageDigitizer = new PADigitizerCmds(this, CustomSettings.InputSelection.VOLTage);

            CURRentDigitizer.CurrentInput = input;
        }

        public double PerformACPowerMeasurementRoutine()
        {
            VOLTageDigitizer.Autoscale();

            VOLTageDigitizer.RangeToFullWaveform();
            CURRentDigitizer.RangeToFullWaveform();
            POWerDigitizer.WaveformCmds.ZoomToFullWaveform();

            this.InstrumentCommands.SetAnalyzeMode(true);
            this.POWerDigitizer.StartPowerQualityMeasurement();

            return this.POWerDigitizer.GetRealACPower();
        }

        public double PerformDCPowerMeasurementRoutine()
        {
            VOLTageDigitizer.Autoscale();

            VOLTageDigitizer.RangeToFullWaveform();
            CURRentDigitizer.RangeToFullWaveform();
            POWerDigitizer.WaveformCmds.ZoomToFullWaveform();

            return this.POWerDigitizer.GetRmsValue();
        }
    }
}
