﻿using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands
{
    public class ScopeChanCmds : ChanCmds // IWaveformDisplayChannelCmds
    {
        new public ScopeCmds InstrumentCommands { get; set; }
        public ScopeDigitizerCmds Digitizer { get; set; }

        public ScopeChanCmds(ScopeCmds _instrumentCmds, int _channel):base(_instrumentCmds, _channel)
        {
            this.InstrumentCommands = _instrumentCmds;
        }
    }
}
