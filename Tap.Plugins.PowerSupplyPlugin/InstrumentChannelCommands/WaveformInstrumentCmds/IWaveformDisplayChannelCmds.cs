﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands
{
    public interface IWaveformDisplayChannelCmds
    {
        IWaveformDisplayInstrCmds InstrumentCommands { get; set; }
    }
}
