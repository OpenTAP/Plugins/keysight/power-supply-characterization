﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    /// <summary>
    /// Represents a channel of an instrument
    /// </summary>
    public abstract class ChanCmds
    {
        //Properties which are only accessible in this class or in classes which derive from InstrumentCannel


        /// <summary>
        /// Channelnumber of the InstrumentChannel.
        /// </summary>
        public int channel;

        public InstrCmds InstrumentCommands;

        public ChanCmds(InstrCmds _InstrumentCommands, int _channel)
        {
            this.channel = _channel;
            this.InstrumentCommands = _InstrumentCommands;
        }

        public void Setchannel(int _channel)
        {
            this.channel = _channel;
        }
    }
}
