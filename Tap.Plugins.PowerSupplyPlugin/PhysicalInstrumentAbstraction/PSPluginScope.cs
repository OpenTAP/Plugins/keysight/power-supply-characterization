﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannelCommands;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.OtherClasses
{
    public class PSPluginScope
    {
        public ScopeDigitizerCmds InputVoltageCmds;
        public ScopeDigitizerCmds OutputVoltageCmds;
        public ScopeDigitizerCmds InputCurrentCmds;
        public ScopeDigitizerCmds OutputCurrentCmds;

        public ScopeCmds InstrumentCmds;

        public PSPluginScope(CustomInstrument _instrument, int inVoltChannel, int outVoltChannel, int inCurrChannel, int outCurrChannel)
        {
            this.InstrumentCmds = new ScopeCmds(_instrument);

            InputVoltageCmds = new ScopeDigitizerCmds(new ScopeChanCmds(InstrumentCmds, inVoltChannel ));
            OutputVoltageCmds = new ScopeDigitizerCmds(new ScopeChanCmds(InstrumentCmds, outVoltChannel));
            InputCurrentCmds = new ScopeDigitizerCmds(new ScopeChanCmds(InstrumentCmds, inCurrChannel));
            OutputCurrentCmds = new ScopeDigitizerCmds(new ScopeChanCmds(InstrumentCmds, outCurrChannel));
        }
    }
}
