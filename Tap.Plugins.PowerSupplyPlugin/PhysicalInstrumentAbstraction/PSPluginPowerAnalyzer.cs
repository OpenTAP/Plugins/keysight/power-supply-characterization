﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.PSPluginRecources
{
    public class PSPluginPowerAnalyzer
    {
        public PAChanCmds InputCmds;
        public PAChanCmds OutputCmds;
        public PACmds InstrumentCmds;

        public PSPluginPowerAnalyzer(MultiChanInstrument _instrument, int inputChannel, int outputChannel)
        {
            InstrumentCmds = new PACmds(_instrument);

            InputCmds = new PAChanCmds(InstrumentCmds, inputChannel, _instrument.GetNewPAChannelCmds(inputChannel).CURRentDigitizer.CurrentInput);
            OutputCmds = new PAChanCmds(InstrumentCmds, outputChannel, _instrument.GetNewPAChannelCmds(outputChannel).CURRentDigitizer.CurrentInput);
        }
    }
}
