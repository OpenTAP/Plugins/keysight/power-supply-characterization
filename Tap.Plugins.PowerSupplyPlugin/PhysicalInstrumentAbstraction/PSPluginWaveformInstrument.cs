﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.PSPluginRecources
{
    //This class holds all the Digitizers which are needed to perform an analysis of a waveform
    public struct PSPluginWaveformInstrumentAdaptor
    {
        public IWaveformDisplayDigitizerCmds InputVoltageDigitizerCmds;
        public IWaveformDisplayDigitizerCmds OutputVoltageDigitizerCmds;
        public IWaveformDisplayDigitizerCmds InputCurrentDigitizerCmds;
        public IWaveformDisplayDigitizerCmds OutputCurrentDigitizerCmds;
        public IWaveformDisplayDigitizerCmds InputPowerDigitizerCmds;
        public IWaveformDisplayDigitizerCmds OutputPowerDigitizerCmds;

        public IWaveformDisplayInstrCmds InstrumentCmds;
    }
}
