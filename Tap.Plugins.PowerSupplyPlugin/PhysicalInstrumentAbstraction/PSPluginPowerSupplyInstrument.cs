﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;
using Tap.Plugins.PowerSupplyPlugin.Multimeter;

namespace Tap.Plugins.PowerSupplyPlugin.PSPluginRecources
{
    public class PSPluginPowerSupplyInstrument
    {
        public CustomInstrument powerSupplyInstrument;
        public IMultimeterDigitizerCmds ImputCurrentMultimeter;

        public PSPluginPowerSupplyInstrument(PSChanCmds _powerSupplyChannel)
        {
            this.powerSupplyInstrument = _powerSupplyChannel.InstrumentCommands.instrument;
            ImputCurrentMultimeter = _powerSupplyChannel.CurrentMultimeter;
        }
    }
}
