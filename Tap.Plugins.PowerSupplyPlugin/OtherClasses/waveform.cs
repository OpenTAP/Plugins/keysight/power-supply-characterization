﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tap.Plugins.PowerSupplyPlugin.OtherClasses
{
    public class waveform
    {
        public List<double> yAxisValues { get; set; }
        public List<double> timeAxisValues { get; set; }
        public double triggerTime { get; set; }

        public waveform()
        {
            yAxisValues = new List<double>();
            timeAxisValues = new List<double>();
        }

        /// <summary>
        /// Gets the time until a singal is in its stationary state
        /// The trigger time is considered to be the beginning of the tuning process
        /// </summary>
        /// <param name="wave">
        /// The waveform to be analyzed
        /// </param>
        /// <param name="upperLimit">
        ///  Highest allowed value for which the signal is still in its stationary state
        /// </param>
        /// <param name="lowerLimit">
        /// Lowest allowed value for which the signal is still in its stationary state
        /// </param>
        /// <returns>
        /// The tune time in seconds
        /// </returns>
        public double GetTuneTime(double upperLimit, double lowerLimit)
        {
            double indexOfLastValueInRange = 0;

            #region get the index of the last value which is out of range
            double index = 0;
            foreach (double d in this.yAxisValues)
            {
                if(index == 57900)
                {
                    index = 57900;
                }

                if (d > upperLimit || d < lowerLimit)
                {
                    indexOfLastValueInRange = index;
                }
                index++;
            }
            #endregion

            double timeOfScreenLength = this.timeAxisValues[this.timeAxisValues.Count-1];
            double AquiredPoints = this.yAxisValues.Count;
            double timeBetweenPoints = timeOfScreenLength / this.yAxisValues.Count;
            double displayEdgeToTrigger = 0;

            displayEdgeToTrigger = (this.yAxisValues.Count / 2) * timeBetweenPoints - triggerTime;


            return (indexOfLastValueInRange * timeBetweenPoints - displayEdgeToTrigger);
        }

        /// <summary>
        /// Gets the time until a singal is in its stationary state
        /// The stationary state is considered as the average value of the last 200 samples on the screen
        /// The trigger time is considered to be the beginning of the tuning process
        /// </summary>
        /// <param name="upperLimit">
        /// Highest allowed value for which the signal is still in its stationary state
        /// </param>
        /// <param name="lowerLimit">
        /// Lowest allowed value for which the signal is still in its stationary state
        /// </param>
        /// <returns>
        /// The tune time in seconds
        /// </returns>
        public double GetTuneTimeWithAbsoluteLimits(double upperLimit, double lowerLimit)
        {  
            return this.GetTuneTime(upperLimit, lowerLimit);
        }

        /// <summary>
        /// Gets the time until a singal is in its stationary state
        /// The stationary state is considered as the average value of the last 200 samples on the screen
        /// The trigger time is considered to be the beginning of the tuning process
        /// </summary>
        /// <param name="triggerTime">
        /// Time between the left edge of the display and the time at which the scope triggers
        /// </param>
        /// <returns>
        /// The tune time in seconds
        /// </returns>
        public double GetTuneTimeWithPercentalLimits(double upperLimitPercentage, double lowerLimitPercentage)
        {
            double stationaryValue = GetAverage(this.yAxisValues.GetRange(this.yAxisValues.Count - 200, 200));    //get the average of the last 200 values

            double upperLimit = stationaryValue * (upperLimitPercentage / 100);       //Set the absolute upper Limit
            double lowerLimit = stationaryValue * (lowerLimitPercentage / 100);       //Set the absolute lower Limit

            return this.GetTuneTime(upperLimit, lowerLimit);
        }

        private double GetAverage(List<double> list)
        {
            double summ = 0;

            foreach (double d in list)
            {
                summ = summ + d;
            }
            return summ / (list.Count);
        }
    }
}
