﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public static class CustomSettings
    {
        //Custom Settings for the AC Power Analyzer
        public enum InputSelection { CURRent, VOLTage, POWer}
        public enum InputNRejectSrcSelection { CURRent, VOLTage };
        public enum VoltCurrSelection { CURRent, VOLTage };
        public enum InputSourceSelection { CURRent, VOLTage, EXTernal, LINE, TRIGger };
        public enum MeasureSourceSelection { VOLTage, CURRent, POWer, FUNCtion, MATH, WMEMemory };
        public enum SlopeSelection { POSitive, NEGative, EITHher, ALTernate };
        public enum TriggerType { EDGE , EVENt, PWIDth, GLITch, PATTern, TV, DELay, EBURst, OR, RUNT, SHOLd, TRANsition, SBUS1, SBUS2, NFC, USB };
        public enum TriggerMode { TRIGgered, AUTO, NORMal }   
        public enum LayoutSelection { FORM1, FORM2, FORM3, FORM4, FORM5, FORM6, FORM7, FORM8 };
        public enum IECLimitSelection { NONE, CLASSA, CLASSB, CLASSC, CLASSD, TABLE2, TABLE3, TABLE4, TABLE5 };
        public enum NoiseRejectLevelSelection { LOW, MEDIUM, HIGH };
        public enum ArbFunctionType { STEP, RAMP, STAircase, SINusoid, PULSe, TRAPezoid, EXPonential, UDEFined, CDWell, SEQuence, NONE };
        public enum MarkerSelection { X1, X2, Y1, Y2, XDELta, YDELta };
        public enum SenseSelection { INTernal, EXTernal };
        public enum TransientModeSelection { FIXed, STEP, LIST, ARB };
        public enum ListResponseSelection { ONCE, AUTO };
        public enum TriggerSourceSelection { BUS, IMMediate, EXTernal, TRANsient1, TRANsient2, TRANsient3, TRANsient4, PIN1, PIN2, PIN3, PIN4, PIN5, PIN6, PIN7};
        public enum ListRepeatCountSelection { INFinity, MINimum, MAXimum };
        public enum RelaySelection { YESRelay, NORelay };
        public enum EMType { PS4Q, PS2Q, PS1Q, BATTery, CHARger, CCLoad, CVLoad, VMETer, AMETer};
        public enum CouplingSelection { AC, DC };
        public enum AutoSetting { AUTO };
        public enum AveragingSetting { APParent, PFACtor, REACtive, REAL };

        //Custom Settings for the Oscilloscope
        public enum ShowOption { ALL, DISPlayed }
        public enum ScopeInputSelection { CHANnel, FUNCtion, MATH, FFT, SBUS1, SBUS2, WMEMory }
        public enum ScopeEdgeSelectMode { MANual, AUTO }

        //Custom Settings for Power Converters
        public enum TmodeOption { HIGHz, LOWz }
    }
}
