﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tap.Plugins.PowerSupplyPlugin.Teststeps
{
    static class Actualize
    {
        public static void ActualizeElectronicLoadCheckbox(this ref bool ElecLoadCheckbox, bool ResistorCheckbox)
        {
            //Electronic Load OR fixed load can be used. Not both.
            if (ResistorCheckbox == true)
            {
                ElecLoadCheckbox = false;
            }
            else if (ResistorCheckbox == false)
            {
                ElecLoadCheckbox = true;
            }
        }

        public static void ActualizeResistorLoadCheckbox(this ref bool ResistorCheckbox, bool ElecLoadCheckbox)
        {
            //Electronic Load OR fixed load is can be used. Not both
            if (ElecLoadCheckbox == true)
            {
                ResistorCheckbox = false;
            }
            else if (ElecLoadCheckbox== false)
            {
                ResistorCheckbox = true;
            }
        }
    }
}
