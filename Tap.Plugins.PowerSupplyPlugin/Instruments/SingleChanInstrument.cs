﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

//Note this template assumes that you have a SCPI based instrument, and accordingly
//extends the ScpiInstrument base class.

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    [Display("Single Channel Instrument", Group: "Instruments for the Power Supply Plugin", Description: "Represents a single channel instrument which can be used for tests of power supplies whith the power supply plugin.")]
    public class SingleChanInstrument : CustomInstrument
    {
        //User defined properties displayed in the instrument settings

        #region declaration loadChannel
        [AvailableValues("InstrumentTypesSelection")]
        [DisplayAttribute("Instrument Type", Group: "Instrument Type", Description: "Type of represented Instrument")]
        public string InstrumentType{ get; set; }
        public List<string> InstrumentTypesSelection
        {
            get
            {
                return new List<string> { strACPowerSupply, strDCPowerSupply, strElectronicLoad, /*strPowerAnalyzer,*/ strOscilloscope };
            }
        }
        #endregion

        //Private properties to determine the available instruments

        private string strACPowerSupply = "AC Power Supply";
        private string strDCPowerSupply = "DC Power Supply";
        private string strElectronicLoad = "Electronic Load";
        private string strPowerAnalyzer = "Power Analyzer";
        private string strOscilloscope = "Oscilloscope";

        public ChanCmds channelCmds;

        //Constructor
        public SingleChanInstrument()
        {
            // ToDo: Set default values for properties / settings.
        }

        public string getInstrumentTypeString()
        {
            return InstrumentType;
        }

        public override PSChanCmds GetNewPSChannelCmds(int channel = -1)
        {
            if (InstrumentType == strACPowerSupply)
            {
                this.channelCmds = new ACPSChanCmds(new ACPSCmds(this), channel);
                return (ACPSChanCmds) this.channelCmds;
            }
            else if (InstrumentType == strDCPowerSupply)
            {
                this.channelCmds = new DCPSChanCmds(new DCPSCmds(this), channel);
                return (DCPSChanCmds) this.channelCmds;
            }

            return null;        //Return null if selected channel does not exist (should not happen)
        }
    }
}
