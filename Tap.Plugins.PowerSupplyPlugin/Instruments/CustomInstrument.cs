﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System.Linq;
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Teststeps;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public abstract class CustomInstrument : ScpiInstrument
    {
        //Constructor
        public CustomInstrument()
        {
            
        }

        public abstract PSChanCmds GetNewPSChannelCmds(int channel);

        public void SendMethod(int timeout, byte[] buffer, long length)
        {
            string str = Encoding.ASCII.GetString(buffer, 0, ((int) length)-1);

            this.ScpiCommand(str);
        }

        public void ReceiveMethod(int timeout, byte[] sendBuffer, long sendLength, out byte[] readBuffer, out int readLength)
        {
            string sendStr = Encoding.ASCII.GetString(sendBuffer,0,((int)sendLength)-1);
            
            string receiveStr = this.ScpiQuery(sendStr);
            readBuffer = Encoding.ASCII.GetBytes( receiveStr);
            readLength = receiveStr.Length;
        }


        public void SetListResponse(int channel, CustomSettings.ListResponseSelection type)
        {
            throw new NotImplementedException();
        }
    }
}
