﻿// Author:      Jan Volpp (Jan.Volpp@keysight.com)
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Keysight.Tap;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

//Note this template assumes that you have a SCPI based instrument, and accordingly
//extends the ScpiInstrument base class.

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    [Display("Multi Channel Instrument", Group: "Instruments for the Power Supply Plugin", Description: "Represents a multi channel instrument which can be used for tests of power supplies whith the power supply plugin.")]
    public class MultiChanInstrument : CustomInstrument
    {
        //User defined properties displayed in the instrument settings

        [Display("Electronic Load Channels", Group: "Usage of Channels")]
        public List<int> LoadChannels { get; set; }

        [Display("DC Power Supply Channels", Group: "Usage of Channels")]
        public List<int> DC_PS_Chans { get; set; }

        [Display("AC Power Supply Channels", Group: "Usage of Channels")]
        public List<int> AC_PS_Chans { get; set; }

        [Display("2 A Power Analyzer Channels", Group: "Usage of Channels")]
        public List<int> PAChans2A { get; set; }

        [Display("50 A Power Analyzer Channels", Group: "Usage of Channels")]
        public List<int> PAChans50A { get; set; }

        [Display("Scope Channels", Group: "Usage of Channels")]
        public List<int> Scope_Chans { get; set; }

        public PACmds powerAnalyzerCmds;
        public ScopeCmds scopeCmds;
        public ELoadCmds eloadCmds;
        public PSCmds psCmds;

        public void InitializeInstrument(string VisaAdress)
        {
            this.VisaAddress = VisaAddress;
            this.Open();

            powerAnalyzerCmds = new PACmds(this);
            scopeCmds = new ScopeCmds(this);
        }

        //Constructor

        public MultiChanInstrument()
        {
            LoadChannels = new List<int> { };
            DC_PS_Chans = new List<int> { };
            AC_PS_Chans = new List<int> { };
            PAChans2A = new List<int> { };
            PAChans50A = new List<int> { };
            Scope_Chans = new List<int> { };


            string doubledChannelMsg = "One channel can only be assigned to one Instrumenttype at a time.";

            Rules.Add(CheckLoadChannels, doubledChannelMsg, "LoadChannels");
            Rules.Add(CheckDC_PS_Chans, doubledChannelMsg, "DC_PS_Chans");
            Rules.Add(CheckAC_PS_Chans, doubledChannelMsg, "AC_PS_Chans");
            Rules.Add(CheckPAChans2A, doubledChannelMsg, "PAChans2A");
            Rules.Add(CheckPAChans50A, doubledChannelMsg, "PAChans50A");
            Rules.Add(CheckScope_Chans, doubledChannelMsg, "Scope_Chans");
        }
 

        //Methods for gathering information about an object of the class

        /// <summary>
        /// Get all the channels which represent a Power Supply.
        /// </summary>
        /// <returns>
        /// List with all the channels which are Power Supplys.
        /// </returns>
        public List<int> GetPowerSupplyChans()
        {
            if(DC_PS_Chans == null)
            {
                DC_PS_Chans = new List<int>{ };
            }
            if(AC_PS_Chans == null)
            {
                AC_PS_Chans = new List<int> { };
            }

            var connectedList = AC_PS_Chans.Concat(DC_PS_Chans);
            List<int> connectedList2 = connectedList.ToList();

            return connectedList2;
        }

        /// <summary>
        /// Get all the channels which represent a Power Analyzer.
        /// </summary>
        /// <returns>
        /// List with all the channels which are Power Analyzers.
        /// </returns>
        public List<int> GetPowerAnalyzerChans()
        {
            List<int> returnList = new List<int> { };
            returnList.AddRange(PAChans2A);
            returnList.AddRange(PAChans50A);

            return returnList;
        }

        /// <summary>
        /// Get all the channels which represent an Oscilloscope.
        /// </summary>
        /// <returns>
        /// List with all the channels which are Oscilloscopes.
        /// </returns>
        public List<int> GetScopeChans()
        {
            return Scope_Chans;
        }



        /// <summary>
        /// Determines the Instrument which uses a known channel.
        /// </summary>
        /// <param name="channel">
        /// Channel which is used to determine the kind of Instrument.
        /// </param>
        /// <returns>
        /// The instrument which was choosen in the instrument settings for a specified channel.
        /// </returns>
        public PAChanCmds GetNewPAChannelCmds(int channel)
        {
            foreach (int i in PAChans2A)
            {
                if (i == channel)
                {
                    return new PAChanCmds( this.powerAnalyzerCmds, channel,PADigitizerCmds.CurrentInputType.A2);
                }
            }
            foreach (int i in PAChans50A)
            {
                if (i == channel)
                {
                    return new PAChanCmds( this.powerAnalyzerCmds, channel,PADigitizerCmds.CurrentInputType.A50);
                }
            }

            return null;        //Return null if selected channel does not exist (should not happen)
        }

        public override PSChanCmds GetNewPSChannelCmds(int channel)
        {
            foreach (int i in AC_PS_Chans)
            {
                if (i == channel)
                {
                    return new ACPSChanCmds(new ACPSCmds(this), channel);
                }
            }
            foreach (int i in DC_PS_Chans)
            {
                if (i == channel)
                {
                    return new DCPSChanCmds(new DCPSCmds(this), channel);
                }
            }

            return null;        //Return null if selected channel does not exist (should not happen)
        }

        //Private Methods
        //Methods to check if the entered channellists are valid

        private bool CheckLoadChannels()
        {
            return CheckOnDoubledChannels(LoadChannels);
        }
        private bool CheckDC_PS_Chans()
        {
            return CheckOnDoubledChannels(DC_PS_Chans);
        }
        private bool CheckAC_PS_Chans()
        {
            return CheckOnDoubledChannels(AC_PS_Chans);
        }
        private bool CheckPAChans2A()
        {
            return CheckOnDoubledChannels(PAChans2A);
        }
        private bool CheckPAChans50A()
        {
            return CheckOnDoubledChannels(PAChans50A);
        }
        private bool CheckScope_Chans()
        {
            return CheckOnDoubledChannels(Scope_Chans);
        }


        //Support Methods for the Check...Methods

        /// <summary>
        /// Checks if the overhanded list of cannels includes a channel which is already stated in another channellist
        /// </summary>
        /// <param name="channelToCheck"></param>
        /// <returns>
        /// false if one of the channels is already stated in another channellist
        /// true if no one of the channels is already stated in another channellist
        /// </returns>
        private bool CheckOnDoubledChannels(List<int> channelListToCheck)
        {
            List<List<int>> allSelectedChannels = new List<List<int>> { LoadChannels, DC_PS_Chans, AC_PS_Chans, PAChans2A, PAChans50A, Scope_Chans };
            
            foreach(List<int> channelList in allSelectedChannels)
            {
                if (channelList != channelListToCheck)
                {
                    if(CheckDoubledItems(channelList, channelListToCheck))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if there is any value which exists in List1 and List2
        /// </summary>
        /// <param name="List1">
        /// First list which contains variables of the type int
        /// </param>
        /// <param name="List2">
        /// Second List which contains variables of the type int
        /// </param>
        /// <returns>
        /// Returns true if List1 contains a value which is equal to a value in List2
        /// Returns false if not
        /// </returns>
        private bool CheckDoubledItems(List<int> List1, List<int> List2)
        {
            foreach (int i in List1)
            {
                foreach (int j in List2)
                {
                    if (j == i)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
