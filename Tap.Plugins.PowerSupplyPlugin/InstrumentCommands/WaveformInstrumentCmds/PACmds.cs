﻿using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentChannels;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public class PACmds : InstrCmds, IWaveformDisplayInstrCmds
    {
        public PACmds(CustomInstrument _instrument): base(_instrument)
        {
            
        }

        /// <summary>
        /// Sets the Instrumment into single trigger mode.
        /// </summary>
        public void SetSingle()
        {
            this.instrument.ScpiCommand(":SINGle");
            TestPlan.Sleep(2000);
        }

        //Acquire Subsystem
        public int GetAcquiredPoints()
        {
            string returnstring;

            returnstring = this.instrument.ScpiQuery("ACQuire:POINts?");

            return int.Parse(returnstring);
        }

        //Tigger Subsystem
        /// <summary>
        /// Selects wether the Trigger occurs on positive, negative or both edges.
        /// </summary>
        /// <param name="type">
        /// New Slopetype to select.
        /// </param>
        public void SetSlope(CustomSettings.SlopeSelection type)
        {
            this.instrument.ScpiCommand(":TRIGger:EDGE:SLOPe " + type.ToString("g"));
        }

        public void SetTriggerType(CustomSettings.TriggerType type)
        {
            this.instrument.ScpiCommand(":TRIGger:MODE " + type.ToString("g"));
        }

        /// <summary>
        /// Selects the Trigger Mode.
        /// </summary>
        /// <param name="type">
        /// Trigger Mode to be set.
        /// </param>
        public void SetTriggerMode(CustomSettings.TriggerMode mode)
        {
            this.instrument.ScpiCommand(":TRIGger:SWEep " + mode.ToString("g"));
        }


        //Timebase Subsystem
        /// <summary>
        /// Sets the scaling in the timebase.
        /// </summary>
        /// <param name="scale">
        /// Timescale in seconds per div.
        /// </param>
        public void SetTimebaseScale(double scale)
        {
            string strscale = scale.ToString();

            this.instrument.ScpiCommand(":TIMebase:SCALe " + strscale);
        }

        public double GetTimebaseScale()
        {
            string str;

            str = this.instrument.ScpiQuery("TIMebase:SCALe?");

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// The reference point (left, right, or center) is set by TIMebase:REFerence.
        /// The maximum position value depends on the time/division settings.
        /// </summary>
        /// <param name="position"></param>
        public void SetTimebasePosition(double position)
        {
            this.instrument.ScpiCommand("TIMebase:POSition " + position.ToString());
        }

        public double GetTimebasePosition()
        {
            string str;

            str = this.instrument.ScpiQuery("TIMebase:POSition?");

            return Convert.ToDouble(str);
        }

        //Display subsystem

        /// <summary>
        /// Sets the Layout of the Display.
        /// </summary>
        /// <param name="type">
        /// Display Format to be set.
        /// </param>
        public void SetDisplayLayout(CustomSettings.LayoutSelection type)
        {
            this.instrument.ScpiCommand("DISPlay:LAYout " + type.ToString("g"));
        }


        //Analyze Subsystem
        /// <summary>
        /// Sets or resets the analyze Mode of the Power Analyzer.
        /// </summary>
        /// <param name="mode">
        /// New state of the Analyze mode.
        /// </param>
        public void SetAnalyzeMode(bool mode)
        {
            string strmode;

            if (mode)
            {
                strmode = "ON";
            }
            else
            {
                strmode = "OFF";
            }
            this.instrument.ScpiCommand("ANALyze:ENABle " + strmode);
        }



        /// <summary>
        /// No documentation yet
        /// </summary>
        /// <param name="frequency"></param>
        public void SetHarmonicsFundamental(double frequency)
        {
            string strfrequency = frequency.ToString();

            this.instrument.ScpiCommand("ANALyze:HARMonics:FUNDamental " + strfrequency);
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        /// <param name="type"></param>
        public void SetHarmonicsLimits(CustomSettings.IECLimitSelection type)
        {
            this.instrument.ScpiCommand("ANALyze:HARMonics:STANdard " + type.ToString("g"));
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        /// <param name="number"></param>
        public void SetHarmonicsNumber(int number)
        {
            string strnumber = number.ToString();

            this.instrument.ScpiCommand("ANALyze:HARMonics:NUMBer " + strnumber);
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        /// <param name="duration"></param>
        public void SetHarmonicsDuration(double duration)
        {
            string strduration = duration.ToString();
            this.instrument.ScpiCommand("ANALyze:HARMonics:DURAtion " + strduration);
        }

        /// <summary>
        /// Adds the specified amount of hysteresis to the specified source signal to prevent the instrument from falsely counting extra edges on a noisy or distorted signal.
        /// </summary>
        /// <param name="nReject"></param>
        /// <param name="rejectionLevel"></param>
        public void SetSyncReject(CustomSettings.InputNRejectSrcSelection nReject, CustomSettings.NoiseRejectLevelSelection rejectionLevel)
        {
            this.instrument.ScpiCommand("ANALyze:SYNC:NREJect " + rejectionLevel.ToString("g") + "," + nReject.ToString("g"));
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        public void MeasHarmonics()
        {
            this.instrument.ScpiCommand("ANALyze:HARMonics:MEASure");
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        public double GetHarmonicsTHD()
        {
            string str = this.instrument.ScpiQuery("ANALyze:HARMonics:THD?");

            return Convert.ToDouble(str);
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        public bool GetHarmonicsStatus()
        {
            string str = this.instrument.ScpiQuery("ANALyze:HARMonics:STATus?");

            if (str == "pass")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// No documentation yet
        /// </summary>
        public void GetHarmonicsData()
        {
            //Needs to be completed

            this.instrument.ScpiQuery("ANALyze:HARMonics:DATA?");
        }

 

        //Marker subsystem

        /// <summary>
        /// If MARKer:MODE is not presently set to WAVEform, this command sets MARKer:MODE to MANual and sets the cursor position to the specified value.
        /// </summary>
        /// <param name="type">
        /// Type of the cursor to get the value from.
        /// </param>
        /// <returns>
        /// The value of the cursor.
        /// </returns>
        public double GetMarker(CustomSettings.MarkerSelection type)
        {
            string str;

            str = this.instrument.ScpiQuery("MARKer:" + type.ToString("g") + "Position?");

            return Convert.ToDouble(str);
        }
    }
}
