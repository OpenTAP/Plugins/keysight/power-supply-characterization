﻿using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public interface IWaveformDisplayInstrCmds
    {
        CustomInstrument instrument { get; set; }

        void SetSingle();
        int GetAcquiredPoints();

        void SetTriggerType(CustomSettings.TriggerType type);
        /// <summary>
        /// Chooses wether the trigger occurs on positive, negative or both edges.
        /// </summary>
        /// <param name="type">
        /// New slopetype to select.
        /// </param>
        void SetSlope(CustomSettings.SlopeSelection type);
        /// <summary>
        /// Selects the Trigger Mode.
        /// </summary>
        /// <param name="type">
        /// Trigger Mode to be set.
        /// </param>
        void SetTriggerMode(CustomSettings.TriggerMode mode);
        /// <summary>
        /// Sets the voltage, current or powerlevel which invokes the trigger
        /// </summary>
        /// <param name="level">
        /// Level to be set in Volts.
        /// </param>
        


        //Timebase Subsystem
        /// <summary>
        /// Sets the offset in the timebase.
        /// </summary>
        /// <param name="offset">
        /// New offset to be set.
        /// </param>
        void SetTimebasePosition(double position);
        /// <summary>
        /// Sets the scaling in the timebase.
        /// </summary>
        /// <param name="scale">
        /// Timescale in seconds per div.
        /// </param>
        void SetTimebaseScale(double scale);

        double GetTimebaseScale();

        double GetTimebasePosition();

        //Marker Subsystem
        double GetMarker(CustomSettings.MarkerSelection type);
    }
}
