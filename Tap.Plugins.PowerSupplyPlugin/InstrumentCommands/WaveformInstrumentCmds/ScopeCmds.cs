﻿using Agilent.CommandExpert.ScpiNet.AgInfiniiVision3000X_7_11;
using Keysight.Tap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.InstrumentCommands;

namespace Tap.Plugins.PowerSupplyPlugin.Instruments
{
    public class ScopeCmds : InstrCmds, IWaveformDisplayInstrCmds
    {
        public ScopeCmds(CustomInstrument _instrument) : base(_instrument)
        {
            device = new AgInfiniiVision3000X(_instrument.SendMethod, _instrument.ReceiveMethod); ;
            ((AgInfiniiVision3000X)device).Transport.DefaultTimeout.Set(this.instrument.IoTimeout);
        }

        /// <summary>
        /// Sets the Instrumment into single trigger mode.
        /// </summary>
        public void SetSingle()
        {
            ((AgInfiniiVision3000X)device).SCPI.SINGle.Command();

            TestPlan.Sleep(2000);
        }

        //Acquire Subsystem
        public int GetAcquiredPoints()
        {
            int returnvalue;
            ((AgInfiniiVision3000X)device).SCPI.ACQuire.POINts.Query(out returnvalue);
            return returnvalue;

        }

        //Trigger Subsystem
        /// <summary>
        /// Chooses wether the trigger occurs on positive, negative or both edges.
        /// </summary>
        /// <param name="type">
        /// New slopetype to select.
        /// </param>
        public void SetSlope(CustomSettings.SlopeSelection type)
        {
            ((AgInfiniiVision3000X)device).SCPI.TRIGger.EDGE.SLOPe.Command(type.ToString("g"));
        }

        /// <summary>
        /// Selects the Event to occur for invoking the Trigger.
        /// </summary>
        /// <param name="type">
        /// Event for the Trigger to be invoked.
        /// </param>
        public void SetTriggerType(CustomSettings.TriggerType type)
        {
            ((AgInfiniiVision3000X)device).SCPI.TRIGger.MODE.Command(type.ToString("g"));
        }

        /// <summary>
        /// Selects the Trigger Mode.
        /// </summary>
        /// <param name="type">
        /// Trigger Mode to be set.
        /// </param>
        public void SetTriggerMode(CustomSettings.TriggerMode type)
        {
            ((AgInfiniiVision3000X)device).SCPI.TRIGger.SWEep.Command(type.ToString("g"));
        }


        //Timebase Subsystem

        /// <summary>
        /// Sets the scaling in the timebase.
        /// </summary>
        /// <param name="scale">
        /// Timescale in seconds per div.
        /// </param>
        public void SetTimebaseScale(double scale)
        {
            ((AgInfiniiVision3000X)device).SCPI.TIMebase.SCALe.Command(scale);
        }

        /// <summary>
        /// The reference point (left, right, or center) is set by TIMebase:REFerence.
        /// The maximum position value depends on the time/division settings.
        /// </summary>
        /// <param name="position">
        /// Position to be set.
        /// </param>
        public void SetTimebasePosition(double position)
        {
            ((AgInfiniiVision3000X)device).SCPI.TIMebase.POSition.Command(position);
        }

        public double GetTimebasePosition()
        {
            double pos = 0;

            ((AgInfiniiVision3000X)device).SCPI.TIMebase.POSition.Query(out pos);

            return pos;
        }

        /// <summary>
        /// Sets the scaling in the timebase.
        /// </summary>
        /// <param name="scale">
        /// Timescale in seconds per div.
        /// </param>
        public double GetTimebaseScale()
        {
            double scale;

            ((AgInfiniiVision3000X)device).SCPI.TIMebase.SCALe.Query(out scale);

            return scale;
        }

        //Marker subsystem
        /// <summary>
        /// If MARKer:MODE is not presently set to WAVEform, this command sets MARKer:MODE to MANual and sets the cursor position to the specified value.
        /// </summary>
        /// <returns>
        /// The value of the cursor.
        /// </returns>
        public double GetMarker(CustomSettings.MarkerSelection type)
        {
            double position = -1000;

            if(type == CustomSettings.MarkerSelection.X1)
            {
                ((AgInfiniiVision3000X)device).SCPI.MARKer.X1.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.X1Position.Query(out position);
            }
            else if(type == CustomSettings.MarkerSelection.X2)
            {
                ((AgInfiniiVision3000X)device).SCPI.MARKer.X2.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.X2Position.Query(out position);
            }
            else if (type == CustomSettings.MarkerSelection.Y1)
            {
                ((AgInfiniiVision3000X)device).SCPI.MARKer.Y1.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.Y1Position.Query(out position);
            }
            else if (type == CustomSettings.MarkerSelection.Y2)
            {
                ((AgInfiniiVision3000X)device).SCPI.MARKer.Y2.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.Y2Position.Query(out position);
            }
            else if (type == CustomSettings.MarkerSelection.XDELta)
            {
                //((AgInfiniiVision3000X)device).SCPI.MARKer.XDELta.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.XDELta.Query(out position);
            }
            else if (type == CustomSettings.MarkerSelection.YDELta)
            {
                //((AgInfiniiVision3000X)device).SCPI.MARKer.XDELta.DISPlay.Command(true);
                ((AgInfiniiVision3000X)device).SCPI.MARKer.YDELta.Query(out position);
            }


            return position;
        }
    }
}
