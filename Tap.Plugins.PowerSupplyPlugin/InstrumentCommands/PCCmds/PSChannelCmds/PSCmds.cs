﻿using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentCommands
{
    public class PSCmds : PCCmds
    {
        public PSCmds(CustomInstrument _instrument) : base(_instrument)
        {

        }

        public enum mode { OFF, LATChing, LIVE }
        public void SetInhibitMode(mode mode)
        {
            if (this.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)device).SCPI.OUTPut.INHibit.MODE.Command(mode.ToString("g"));
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public enum function { DIO, DINPut, FAULt, INHibit, ONCouple, OFFCouple, TINPut, TOUTput}
        public void SetDigitalPinFunction(int pin, function function)
        {
            if(this.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)device).SCPI.SOURce.DIGital.PIN.FUNCtion.Command(pin, function.ToString("g"));
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public enum polarity { POSitive, NEGative}
        public void SetDigitalPinPolarity(int pin, polarity polarity)
        {
            if (this.device.GetType() == typeof(AgN6705))
            {
                ((AgN6705)device).SCPI.SOURce.DIGital.PIN.POLarity.Command(pin, polarity.ToString("g"));
            }
            else
            {
                throw new NotSupportedException();
            }
        }

    }
}