﻿using Agilent.CommandExpert.ScpiNet.AgAC6800_A_01_00_0067;
using Agilent.CommandExpert.ScpiNet.AgN6705_D_02_13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentCommands
{
    public class PCCmds : InstrCmds
    {
        public PCCmds(CustomInstrument _instrument) : base(_instrument)
        {
            if (_instrument.IdnString.Contains("N6705C"))
            {
                device = new AgN6705(_instrument.SendMethod, _instrument.ReceiveMethod);
                ((AgN6705)device).Transport.DefaultTimeout.Set(this.instrument.IoTimeout);
            }
            else if (_instrument.IdnString.Contains("AC6801B"))
            {
                //_instrument.Open();
                device = new AgAC6800(_instrument.SendMethod, _instrument.ReceiveMethod);
                ((AgAC6800)device).Transport.DefaultTimeout.Set(this.instrument.IoTimeout);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
