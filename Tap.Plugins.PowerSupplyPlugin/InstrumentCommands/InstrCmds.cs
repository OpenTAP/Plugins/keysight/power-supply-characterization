﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tap.Plugins.PowerSupplyPlugin.Instruments;

namespace Tap.Plugins.PowerSupplyPlugin.InstrumentCommands
{
    public abstract class InstrCmds
    {
        public IDisposable device;

        public CustomInstrument instrument { get; set; }

        public InstrCmds(CustomInstrument _instrument)
        {
            this.instrument = _instrument;
        }
    }
}
